<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-03-19 10:25
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'] . "/");
if (!class_exists('SQL')) {
    include(ROOT_PATH . "public/dao/db.class.php");
}
include(ROOT_PATH . "public/model/OperateResult.php");
include(ROOT_PATH . "public/model/WebConfigInfo.php");

class UseCountService
{
    /**
     * 获取接口调用次数
     * @return false|string
     */
    function getApiUseCount()
    {
        self::updateUseCount();
        $sql = "SELECT usecount FROM  `sixcloud_config` where id=?";
        $rows = SQL::Read($sql, array(1));
        $isRet = count($rows) > 0;
        return OperateResult::outArray($isRet, $isRet ? null : "获取失败", $isRet ? $rows[0] : null);
    }

    /**
     *  接口调用次数+1
     * ALTER TABLE `newsqxt`.`sixcloud_config`
     * CHANGE COLUMN `version` `usecount` int(255) NULL DEFAULT 0 COMMENT '接口使用次数' AFTER `contact`;
     */
    function updateUseCount()
    {
        $sql = "UPDATE `sixcloud_config` SET `usecount` =usecount+1 WHERE `id` = ?";
        SQL::Write($sql, array(1));
    }
}