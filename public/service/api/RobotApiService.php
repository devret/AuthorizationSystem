<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-19 23:22
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

$ROOT_PATH = $_SERVER['DOCUMENT_ROOT'];
include($ROOT_PATH . "/api.inc.php");
include($ROOT_PATH . "/public/model/AuthInfo.php");
include($ROOT_PATH . "/public/model/KmInfo.php");

class RobotApiService
{

    public function chenkApi($robot, $key)
    {
        $sql = "SELECT * FROM  `sixcloud_infos` where auth=?";
        $rows = SQL::Read($sql, array($robot));

        if (count($rows) <= 0) {
            return OperateResult::outArray(false, "当前机器人没有授权,禁止调用接口！", "");
        }
        if ($rows[0]['status'] != 1) {
            return OperateResult::outArray(false, "当前机器人授权被禁用,禁止调用接口!", "");
        }
        if ($rows[0]['api'] != 1) {
            return OperateResult::outArray(false, "当前机器人无API权限,禁止调用接口！", "");
        }
        if ($rows[0]['expiredate'] != 1 && $rows[0]['expiredate'] < NOW_DATE) {
            return OperateResult::outArray(false, "当前机器人授权已到期,禁止调用接口！", "");
        }
        if ($rows[0]['key'] != $key) {
            return OperateResult::outArray(false, "当前机器人key错误,禁止调用接口！", "");
        }
        return OperateResult::outArray(true, "信息验证通过！", "");
    }

    /**
     * 检测帐号是否存在授权
     * @param $auth 授权帐号
     * @return bool
     */
    public function findSqByAuth($auth)
    {
        $sql = "SELECT * FROM  `sixcloud_infos` where auth=?";
        $rows = SQL::Read($sql, array($auth));
        if (count($rows) > 0) {
            return OperateResult::outArray(true, "", $rows[0]);
        } else {
            return OperateResult::outArray(false, "帐号未授权！", "");
        }
    }

    /**
     * 添加永久授权
     * @param $authorizer
     * @param $robot
     * @return bool
     */
    public function sqtj($AuthInfo)
    {
        $sql = "INSERT INTO `sixcloud_infos`( `authorizer`, `auth`, `key`, `expiredate`, `createdate`,  `remark`) VALUES ( ?, ?, ?, ?,?,?)";
        $data = array(
            $AuthInfo->authorizer,
            $AuthInfo->auth,
            Utils::randomkeys(16),
            1,
            NOW_DATE,
            "来源于机器人授权"
        );
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, "", "");
    }


    /**
     * 检测主人帐号授权列表
     * @param $auth 授权帐号
     * @return bool
     */
    public function findSqByZrQQ($zr)
    {
        $sql = "SELECT * FROM  `sixcloud_infos` where authorizer=?";
        $rows = SQL::Read($sql, array($zr));
        if (count($rows) > 0) {
            return OperateResult::outArray(true, "", $rows);
        } else {
            return OperateResult::outArray(false, "你还没有授权帐号！", "");
        }
    }

    /**
     * 通过机器人号和机器人主人号删除授权信息
     */
    public function deleteSqByZrqqAndQQ($AuthInfo)
    {
        // die(var_dump($AuthInfo));
        $sql = "SELECT * FROM `sixcloud_infos` where authorizer=? and auth = ?";
        $data = array($AuthInfo->authorizer, $AuthInfo->auth);
        $rows = SQL::Read($sql, $data);
        //  die(var_dump($rows));
        if (count($rows) <= 0) {
            return OperateResult::outArray(false, "该账号没有授权!", "");
        }
        //查询到相关信息，执行删除操作
        $sql = "Delete FROM `sixcloud_infos` where authorizer=? and auth = ?";
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, $isSuccess ? "删除授权成功!" : "删除授权失败!", "");
    }

    /**
     * 删除任意授权帐号 - 一般权限是管理员
     */
    public function deleteSqBySqQQ($AuthInfo)
    {

        $sql = "SELECT * FROM `sixcloud_infos` where  auth = ?";
        $data = array($AuthInfo->auth);
        $rows = SQL::Read($sql, $data);

        if (count($rows) <= 0) {
            return OperateResult::outArray(false, "该账号没有授权!", "");
        }
        //查询到相关信息，执行删除操作
        $sql = "Delete FROM `sixcloud_infos` where  auth = ?";
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, $isSuccess ? "删除授权成功!" : "删除授权失败!", "");
    }


    /**
     * 机器人主人更换自己的授权帐号
     */
    public function updateSqByZrQQAndQQ($AuthInfo, $NewQQ)
    {

        //查询需要更换的旧授权信息
        $sql = "SELECT * FROM `sixcloud_infos` where authorizer=? and auth = ?";
        $data = array($AuthInfo->authorizer, $AuthInfo->auth);
        $rows = SQL::Read($sql, $data);

        if (count($rows) <= 0) {
            return OperateResult::outArray(false, "该账号没有授权!", "");
        }

        //查询需要更换的新授权信息
        $sql = "SELECT * FROM `sixcloud_infos` where  auth = ?";
        $data = array($NewQQ);
        $rows = SQL::Read($sql, $data);

        if (count($rows) > 0) {
            return OperateResult::outArray(false, "新帐号已存在授权,请重新更换!", "");
        }

        //所有条件满足，执行更新操作
        $sql = "UPDATE `sixcloud_infos` SET `auth` = ?  WHERE `auth` =?";
        $data = array($NewQQ, $AuthInfo->auth);
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, $isSuccess ? "更换授权成功!" : "更换授权失败!", "");
    }

    /**
     * 更换授权 - 一般权限要求是管理员
     */
    public function updateSqBySqQQ($AuthInfo, $NewQQ)
    {

        //查询需要更换的旧授权信息
        $sql = "SELECT * FROM `sixcloud_infos` where  auth = ?";
        $data = array($AuthInfo->auth);
        $rows = SQL::Read($sql, $data);

        if (count($rows) <= 0) {
            return OperateResult::outArray(false, "该账号没有授权!", "");
        }

        //查询需要更换的新授权信息
        $sql = "SELECT * FROM `sixcloud_infos` where  auth = ?";
        $data = array($NewQQ);
        $rows = SQL::Read($sql, $data);

        if (count($rows) > 0) {
            return OperateResult::outArray(false, "新帐号已存在授权,请重新更换!", "");
        }

        //条件满足,执行更新操作
        $sql = "UPDATE `sixcloud_infos` SET `auth` = ?  WHERE `auth` =?";
        $data = array($NewQQ, $AuthInfo->auth);
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, $isSuccess ? "更换授权成功!" : "更换授权失败!", "");
    }

    /**
     * 更换授权主人 - 一般权限要求是管理员
     */
    public function updateSqZrQQ($AuthInfo, $NewZrQQ)
    {

        //查询需要更换的旧授权信息
        $sql = "SELECT * FROM `sixcloud_infos` where  auth = ?";
        $data = array($AuthInfo->auth);
        $rows = SQL::Read($sql, $data);

        if (count($rows) <= 0) {
            return OperateResult::outArray(false, "该账号没有授权!", "");
        }

        //条件满足,执行更新操作
        $sql = "UPDATE `sixcloud_infos` SET `authorizer` = ?  WHERE `auth` =?";
        $data = array($NewZrQQ, $AuthInfo->auth);
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, $isSuccess ? "更换主人成功!" : "更换主人失败!", "");
    }

    /**
     * 删除某帐号下的所有授权
     * @param $AuthInfo
     */
    function deleteAllSqByZr($AuthInfo)
    {
        $sql = "delete from `sixcloud_infos` where `authorizer`=?";
        $data = array($AuthInfo->authorizer);
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, $isSuccess ? "清空成功!" : "清空失败!", "");
    }


    /**
     * 获取key
     * @param $AuthInfo
     */
    function getKey($AuthInfo)
    {
        $sql = "SELECT * FROM  `sixcloud_infos` where auth = ?";
        $data = array($AuthInfo->auth);
        $rows = SQL::Read($sql, $data);

        if (count($rows) <= 0) {
            return OperateResult::outArray(false, "获取失败,该账号没有授权!", "");
        }

        if ($rows[0]['authorizer'] != $AuthInfo->authorizer) {
            return OperateResult::outArray(false, "获取失败,非所查询机器人的主人!", "");
        }
        return OperateResult::outArray(true, "获取成功！", $rows[0]);
    }


    /**
     * 添加永久卡密
     */
    function addLastingKm($num)
    {
        $bathData = Array();
        for ($i = 0; $i < $num; $i++) {
            $bathData[$i][0] = "INSERT INTO `sixcloud_kms`(`createuserid`, `card`, `use`, `expiredate`, `createdate`) VALUES (?, ?,?, ?, ?)";
            $bathData[$i][1] = array(
                1,
                Utils::getkm(18),
                0,
                0,
                NOW_DATE);
        }
        $isRet = SQL::batchWrite($bathData);
        return OperateResult::outArray($isRet, $isRet ? "生成卡密成功!" : "生成卡密失败！", $isRet ? $bathData : null);
    }

    /**
     * 生成期限卡密
     * @param $num
     * @param $expiredate
     * @return array
     */
    function addTimeKm($num, $expiredate)
    {
        $bathData = Array();
        for ($i = 0; $i < $num; $i++) {
            $bathData[$i][0] = "INSERT INTO `sixcloud_kms`(`createuserid`, `card`, `use`, `expiredate`, `createdate`) VALUES (?, ?,?, ?, ?)";
            $bathData[$i][1] = array(
                1,
                Utils::getkm(18),
                0,
                $expiredate,
                NOW_DATE);
        }
        $isRet = SQL::batchWrite($bathData);

        return OperateResult::outArray($isRet, $isRet ? "生成卡密成功!" : "生成卡密失败！", $isRet ? $bathData : null);
    }

    /**
     * 冻结授权
     * @param $AuthInfo
     * @return array
     */
    function sqStop($AuthInfo){
        $sql = "UPDATE  `sixcloud_infos` SET `status` = ? WHERE  `auth` = ?";
        $data = array(0,$AuthInfo->auth);
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, $isSuccess ? "冻结成功!" : "冻结失败!", "");
    }

    /**
     * 激活授权
     * @param $AuthInfo
     * @return array
     */
    function  sqActivation($AuthInfo){
        $sql = "UPDATE  `sixcloud_infos` SET `status` = ? WHERE  `auth` = ?";
        $data = array(1,$AuthInfo->auth);
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::outArray($isSuccess, $isSuccess ? "激活成功!" : "激活失败!", "");
    }

    /**
     * 卡密查询
     * @param $KmInfo
     * @return array
     */
	function findKmInfo($KmInfo){
		$sql = 'SELECT * FROM  `sixcloud_kms` WHERE `card` = ?';
        $data = array($KmInfo->card);
        $rows = SQL::Read($sql, $data);
        if (count($rows) <= 0) {
            return OperateResult::outArray(false, "卡密不存在!", null);
        }
        return OperateResult::outArray(true, "查询成功", $rows[0]);
	}
}