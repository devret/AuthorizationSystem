<?php
define('ROOT', str_replace('', '', realpath(dirname(__FILE__) . '/../../../')) . "/");
include(ROOT . "api.inc.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/AdminInfo.php");

class LoginService
{
    /**
     * 管理员登录
     * @param $AdminInfo 管理员帐号参数
     * @return false|string
     */
    public function isLogin($AdminInfo)
    {
        $passmd5 = md5(md5($AdminInfo->password));
        $rows = SQL::Read('SELECT * FROM `sixcloud_admin` where username=?', array($AdminInfo->username));

        if (count($rows) == 0) {
            return OperateResult::out(false, "帐号不存在！", null);
        }
        if ($rows[0]['password'] != $passmd5) {
            return OperateResult::out(false, "密码错误！", null);
        } else {

            $session=md5($AdminInfo->username.$passmd5.SYSTEM_KEY.$passmd5);
            $token=authcode("{$AdminInfo->username}\t{$session}", 'ENCODE', SYSTEM_KEY);
           // setcookie("sixcloud_token", $token, time() + 604800);

            session_start();
            $_SESSION['sixcloud_token']=$token;
            $_SESSION['id']=$rows[0]['id'];
            $_SESSION['username']=$AdminInfo->username;
            $_SESSION['password']=$passmd5.SYSTEM_KEY.$passmd5;
            $_SESSION['webConfig']=self::ConfigGetByAdmin();
          //  die(var_dump($_SESSION));
           // $_SESSION['webConfig']['mysqlVersion']=self::getMysqlVersionByAdmin()['mysqlVersion'];
            return OperateResult::out(true, "登录成功！", array("logininfo"=>$rows[0],"cookie"=>$token));
        }
    }

   public function Logout(){
        session_start();
       $_SESSION = array();
       //setcookie("sixcloud_token", "", time() - 604800);
       return OperateResult::out(true, "注销成功！",null);
   }

    /**
     * 获取网站配置参数
     * @return false|string
     */
  public  function ConfigGetByAdmin(){
        $sql = "SELECT *  ,VERSION() as mysqlVersion FROM  `sixcloud_config` where id=?";
        $rows = SQL::Read($sql, array(1));
        return $rows[0];
    }
}

?>