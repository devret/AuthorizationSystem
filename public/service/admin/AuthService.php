<?php
define('ROOT', str_replace('', '', realpath(dirname(__FILE__) . '/../../../')) . "/");
include(ROOT . "session.inc.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/AuthInfo.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/DataGridData.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/PageBean.php");

class AuthService
{
    /**
     * 通过管理员手动授权
     * @param $AuthInfo 授权信息参数
     * @return Object 操作结果
     */
    public function addAuthByAdmin($AuthInfo)
    {
        $isPresence = self::isCheckByAuth($AuthInfo->auth);
        if ($isPresence) {
            return OperateResult::out(false, "该帐号已经有授权！", null);
        }
        if ($AuthInfo->expiredate == "") {
            $AuthInfo->expiredate = '1';
        }
        $sql = "INSERT INTO `sixcloud_infos`(`authorizer`, `auth`,`key`, `expiredate`,`createdate`,`status`,`api`, `remark`) VALUES (?, ?,?,?,?,?,?,?)";
        $data = array(
            $AuthInfo->authorizer,
            $AuthInfo->auth,
            Utils::randomkeys(16),
            $AuthInfo->expiredate,
            NOW_DATE,
            $AuthInfo->status,
            $AuthInfo->api,
            $AuthInfo->remark);
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::out($isSuccess, $isSuccess ? "添加成功！" : "添加失败", null);
    }

    /**
     * 管理员更新某授权信息
     * @param $AuthInfo
     * @return false|string
     */
    public function authInfoUpdateByAdmin($AuthInfo)
    {
        if ($AuthInfo->expiredate == "") {
            $AuthInfo->expiredate = '1';
        }

        //查询需要修改的原始数据信息,分别执行不同语句
        $oldSql = "SELECT * FROM `sixcloud_infos` where `id`=?";
        $rows = SQL::Read($oldSql, array($AuthInfo->id));
        if ($rows[0]['auth'] == $AuthInfo->auth) {
            //修改原授权号信息
            $sql = "UPDATE `sixcloud_infos` SET `authorizer` = ?,  `expiredate` = ?,  `status` = ?, `api` = ?, `remark` = ? WHERE `id` = ?";
            $data = array(
                $AuthInfo->authorizer,
                $AuthInfo->expiredate,
                $AuthInfo->status,
                $AuthInfo->api,
                $AuthInfo->remark,
                $AuthInfo->id);
        } else {
            //修改原授权号以及授权号信息
            $sql = "UPDATE `sixcloud_infos` SET `authorizer` = ?, `auth` = ?, `expiredate` = ?,  `status` = ?,  `api` = ?, `remark` = ? WHERE `id` = ?";
            $data = array(
                $AuthInfo->authorizer,
                $AuthInfo->auth,
                $AuthInfo->expiredate,
                $AuthInfo->status,
                $AuthInfo->api,
                $AuthInfo->remark,
                $AuthInfo->id);
        }
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::out($isSuccess, $isSuccess ? "更新成功！" : "更新失败", null);
    }

    /**
     * 管理通过id删除授权信息
     * @param $id 需要删除授权的id
     * @return false|string
     */
    public function authInfoDeleteByAdmin($idArray)
    {

        $sucCount = 0;
        $failureCount = 0;
        $sql = "DELETE FROM `sixcloud_infos` WHERE `id` = ?";
        $arrLength = count($idArray);
        for ($i = 0; $i < $arrLength; $i++) {
            $isSuccess = SQL::Write($sql, array($idArray[$i]));
            $isSuccess ? $sucCount++ : $failureCount++; //计算删除成功和失败的
        }
        $isRet = $sucCount > 0;
        return OperateResult::out($isRet, $isRet ? "删除成功！" : "删除失败！", $isRet ? array("sucCount" => $sucCount, "failCount" => $failureCount) : null);

    }

    /**
     * 检测是否已经存在授权
     * @param $auth 需要授权的帐号
     * @return bool true-存在、false-不存在
     */
    function isCheckByAuth($auth)
    {
        $sql = "SELECT * FROM `sixcloud_infos` where `auth`=?";
        $rows = SQL::Read($sql, array($auth));
        if (count($rows) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 管理员查看授权帐号列表,可选择查询，分页参数必须传入
     * @param $AuthInfo 授权参数
     * @param $Page 分页参数
     * @return false|string
     */
    public function getAllAuthByAdmin($AuthInfo, $Page)
    {
        $DataGridData = new DataGridData();
        $data = array();
        $sql = "";
        $isSearcch = false;
        if ($AuthInfo->id != "") {
            $isSearcch = true;
            $sql .= " and id = ?";
            array_push($data, $AuthInfo->id);
        }
        if ($AuthInfo->authkm != "") {
            $isSearcch = true;
            $sql .= " and authkm = ?";
            array_push($data, $AuthInfo->authkm);
        }
        if ($AuthInfo->authorizer != "") {
            $isSearcch = true;
            $sql .= " and authorizer = ?";
            array_push($data, $AuthInfo->authorizer);
        }
        if ($AuthInfo->auth != "") {
            $isSearcch = true;
            $sql .= " and auth = ?";
            array_push($data, $AuthInfo->auth);
        }
        if ($AuthInfo->remark != "") {
            $isSearcch = true;
            $sql .= " remark LIKE '%?%'";
            array_push($data, $AuthInfo->remark);
        }
        if ($AuthInfo->status != "") {
            $isSearcch = true;
            $sql .= " and status = ?";
            array_push($data, $AuthInfo->status);
        }
        $sql .= " ORDER BY createdate DESC LIMIT ?,? ";
        array_push($data, $Page->page, (int)$Page->size);

        //获取总数-分有条件和无条件的
        if ($isSearcch == true) {
            $sql1 = "SELECT count(*) as count FROM `sixcloud_infos` WHERE 1=1" . $sql;
            $rows = SQL::Read($sql1, $data);
        } else {
            $sql1 = "SELECT count(*) as count FROM `sixcloud_infos` WHERE 1=1";
            $rows = SQL::Read($sql1, null);
        }

        $DataGridData->count = (count($rows) > 0 ? $rows[0]['count'] : 0);

        //获取所有授权信息
        $sql2 = "SELECT * FROM `sixcloud_infos` WHERE 1=1" . $sql;
        $rows = SQL::Read($sql2, $data);
        if (count($rows) > 0) {
            $DataGridData->code = 0;
        } else {
            $DataGridData->code = 0;
        }
        //调试打开
        // $DataGridData->msg = array($sql1,$sql2,$data);
        $DataGridData->data = $rows;
        return json_encode($DataGridData);
    }

    /**
     * 清空所有授权
     * @return false|string
     */
    function authAllDeleteByAdmin()
    {
        $sql = "TRUNCATE TABLE `sixcloud_infos`";
        $isSuccess = SQL::Write($sql, null);
        return OperateResult::out(true, true ? "清空成功！" : "清空失败", null);
    }

    /**
     * 通过机器人的key生成接口文本
     * @param $key
     * @return false|string
     */
    function exportRobotApiText($key)
    {
        return OperateResult::out(true,  "导出成功！" , Utils::exportRobotApiText($key));
    }
}

?>