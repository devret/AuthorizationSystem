<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-12 12:34
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */
define('ROOT', $_SERVER['DOCUMENT_ROOT'] . "/");
include(ROOT . "session.inc.php");
include(ROOT . "public/model/WebConfigInfo.php");

class ConfigService
{
    /**
     * 管理员修改网站配置参数
     * @param $WebConfigInfo 网站配置参数
     * @return false|string
     */
    function ConfigUpdateByAdmin($WebConfigInfo)
    {

        $sql = "UPDATE `sixcloud_config` 
                                    SET 
                                    `headtitle` =?,
                                     `foottitle` =?, 
                                     `music` = ?, 
                                     `contact` = ? ,
                                     `template` =?
                                     WHERE `id` = 1";
        $data = array(
            $WebConfigInfo->headtitle,
            $WebConfigInfo->foottitle,
            $WebConfigInfo->music,
            $WebConfigInfo->contact,
            $WebConfigInfo->template);
        $isSuccess = SQL::Write($sql, $data);
        return OperateResult::out($isSuccess, $isSuccess ? "修改成功！" : "修改失败", null);
    }

    /**
     * 获取网站配置信息
     * @return false|string
     */
    function ConfigGetByAdmin(){
        $sql = "SELECT * FROM  `sixcloud_config` where id=?";

        $rows = SQL::Read($sql, array(1));
        $rows[0]['dir'] = self::getDir(ROOT_PATH."/index/");
        $isRet=count($rows)>0;
        return OperateResult::out($isRet, $isRet ? null : "获取失败", $isRet ? $rows[0] : null);
    }

    /**
     * 获取统计数据信息
     */
    function getDataInfo(){
            $sql ="SELECT 
                            ( 
                            SELECT 
                            COUNT(id )
                            FROM 
                            sixcloud_infos
                            )  as authCount,
                            
                            ( 
                            SELECT 
                            COUNT(if(`status`=1,true,null)) 
                            FROM 
                            sixcloud_infos 
                            ) as normalCount,
                            
                            ( 
                            SELECT 
                            COUNT(if(`status`=0,true,null)) 
                            FROM 
                            sixcloud_infos 
                            ) as disableCount,
                            
                            ( 
                            SELECT 
                            COUNT(id) 
                            FROM 
                            sixcloud_kms 
                            ) as kmCount,
                            
                            ( 
                            SELECT 
                            COUNT(if(`use`=0,true,null))
                            FROM 
                            sixcloud_kms 
                            ) as noUseCount,
                            
                            ( 
                            SELECT 
                            COUNT(if(`use`=1,true,null))
                            FROM 
                            sixcloud_kms 
                            ) as UseCount
                            ; ";
        $rows = SQL::Read($sql, null);
        $isRet=count($rows)>0;
        return OperateResult::out($isRet, $isRet ? "获取统计信息成功！" : "获取统计信息失败！", $isRet ? $rows[0] : null);
    }

    /**
     * @param $dir 文件夹路径
     * @return array
     */
   private function getDir($dir) {
        $dirArray[]=NULL;
        if (false != ($handle = opendir ( $dir ))) {
            $i=0;
            while ( false !== ($file = readdir ( $handle )) ) {
                //去掉"“.”、“..”以及带“.xxx”后缀的文件
                if ($file != "." && $file != ".."&&!strpos($file,".")) {
                    $dirArray[$i]=$file;
                    $i++;
                }
            }
            //关闭句柄
            closedir ( $handle );
        }
        return $dirArray;
    }
}