<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-15 17:28
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */
define('ROOT', str_replace('', '', realpath(dirname(__FILE__) . '/../../../')) . "/");
include(ROOT . "api.inc.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/AdminInfo.php");
class UserInfoService
{
    /**
     * 通过id修改管理员帐号密码
     * @param $AdminInfo
     * @return false|string
     */
       public function AdminInfoUpdateByAdmin($AdminInfo){
           $passmd5 = md5(md5($AdminInfo->password));
            $sql="UPDATE `sixcloud_admin` SET `username` = ?, `password` = ? WHERE `id` = ?";
            $data = array($AdminInfo->username,$passmd5,$AdminInfo->id);
           $isSuccess = SQL::Write($sql, $data);
         return  OperateResult::out($isSuccess, $isSuccess  ? "修改成功！" : "修改失败！", null);
       }
}