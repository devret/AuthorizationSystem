<?php
/**
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-12 1:26
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */
define('ROOT', str_replace('', '', realpath(dirname(__FILE__) . '/../../../')) . "/");
include(ROOT . "session.inc.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/KmInfo.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/DataGridData.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/PageBean.php");

class KmService
{
    /**
     * 创建卡密
     * @param $KmInfo  要生的卡密信息包含生成人，到期时间
     * @param $CreateNum    需要创建卡密的数量
     * @return false|string
     */
    public function KmCreateByAdmin($KmInfo, $CreateNum)
    {
        if ($KmInfo->expiredate == "") {
            //0代表永久卡密
            $KmInfo->expiredate = '0';
        }
        $bathData = Array();
        for ($i = 0; $i < $CreateNum; $i++) {
            $bathData[$i][0] = "INSERT INTO `sixcloud_kms`(`createuserid`, `card`, `use`, `expiredate`, `createdate`) VALUES (?, ?,?, ?, ?)";
            $bathData[$i][1] = array(
                $KmInfo->createuserid,
                Utils::getkm(18),
                0,
                $KmInfo->expiredate,
                NOW_DATE);
        }
        $isRet = SQL::batchWrite($bathData);

        return OperateResult::out($isRet, $isRet ? "生成卡密成功!" : "生成卡密失败！", $isRet ? $bathData : null);
    }

    /**
     * 管理员根据卡密id删除卡密
     * @param $KmInfo 包含卡密id
     * @return false|string
     */
    public function KmDeleteByAdmin($idArray)
    {
        $sucCount = 0;
        $failureCount = 0;
        $idArrayLength = count($idArray);
        $sql = "DELETE FROM `sixcloud_kms` WHERE `id` = ?";
        for ($i = 0; $i < $idArrayLength; $i++) {
            $isSuccess = SQL::Write($sql, array($idArray[$i]));
            $isSuccess ? $sucCount++ : $failureCount++; //计算删除成功和失败的
        }
        $isRet = $sucCount > 0;
        return OperateResult::out($isRet, $isRet ? "删除成功！" : "删除失败！", $isRet ? array("sucCount" => $sucCount, "failCount" => $failureCount) : null);
    }

    /**
     * 清空所有卡密
     * @return false|string
     */
    public function KmAllDeleteByAdmin()
    {
        $sql = "TRUNCATE TABLE `sixcloud_kms`";
        $isSuccess = SQL::Write($sql, null);
        return OperateResult::out(true, true ? "清空成功！" : "清空失败", null);
    }

    /**
     * 管理员获取全部卡密数据
     * @param $Page  分页参数
     * @return false|string
     */
    public function getAllKmByAdmin($Page)
    {
        $DataGridData = new DataGridData();
        //获取卡密总数
        $sql = "SELECT count(*) as count FROM `sixcloud_kms`";
        $rows = SQL::Read($sql, null);
        $DataGridData->count = (count($rows) > 0 ? $rows[0]['count'] : 0);

        //获取卡密详细信息
        $sql = "SELECT
                    kms.*,
                    adm.username as createusername,
                    info.id AS infoId,
                    info.auth,
                    info.expiredate AS infoExpiredate,
                    info.createdate AS infoCreatedate 
                    FROM
                    `sixcloud_kms` as kms
                    LEFT JOIN sixcloud_infos as info ON kms.card = info.authkm 
                    LEFT JOIN sixcloud_admin as adm on kms.createuserid = adm.id
                    ORDER BY  kms.createdate DESC,usedate DESC   LIMIT ?,?";

        $rows = SQL::Read($sql, array((integer)$Page->page, (integer)$Page->size));
        $DataGridData->code = 0;
        $DataGridData->data = $rows;
        return json_encode($DataGridData);
    }
}

?>