<?php
/**
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-12 0:21
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

class Utils
{
    /**
     * 生成随机字符串
     * @param $length 多少长度的字符串
     * @return string
     */
    public static function randomkeys($length)
    {
        $retStr = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
        for ($i = 0; $i < $length; $i++) {
            $retStr .= $pattern{mt_rand(0, 61)};
        }
        return $retStr;
    }

    /**
     * 生成卡密
     * @param int $length 多少长度的卡密
     * @return string
     */
    public static function getkm($length = 18)
    {
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $strlen = strlen($str);
        $randstr = "";
        for ($i = 0; $i < $length; $i++) {
            $randstr .= $str[mt_rand(0, $strlen - 1)];
        }
        return $randstr;
    }

    /**
     * @param $key
     * @return array按照机器人key导出文本
     */
    public static function exportRobotApiText($key)
    {
        $host = $_SERVER['HTTP_HOST'];
        $apiArray = array(
            '授权冻结* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqdj&key=' . $key . '&qq=$回声段落1$】%机器人主人要求是(权限要求:机器人主人)%'
        , '授权激活* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqjh&key=' . $key . '&qq=$回声段落1$】%机器人主人要求是(权限要求:机器人主人)%'
        , '生成期限卡密* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=kmaddb&key=' . $key . '&num=$回声段落1$&day=$回声段落2$】%创始人要求是(权限要求:创始人)%'
        , '生成永久卡密* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=kmadda&key=' . $key . '&num=$回声段落1$】%创始人要求是(权限要求:创始人)%'
        , '获取key* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=getkey&key=' . $key . '&qq=$回声段落1$】'
        , '清空授权* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqclear&key=' . $key . '&zrqq=$回声段落1$】%创始人要求是(权限要求:创始人)%'
        , '更换主人* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqghzr&key=' . $key . '&qq=$回声段落1$&newzrqq=$回声段落2$】%机器人主人要求是(权限要求:机器人主人)%'
        , '更换授权* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqgh&key=' . $key . '&qq=$回声段落1$&newqq=$回声段落2$】%机器人主人要求是(权限要求:机器人主人)%'
        , '更换我的授权* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqghmy&key=' . $key . '&qq=$回声段落1$&newqq=$回声段落2$】'
        , '删除授权* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqsc&key=' . $key . '&qq=$回声段落1$】%机器人主人要求是(权限要求:机器人主人)%'
        , '删除我的授权* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqscmy&key=' . $key . '&qq=$回声段落1$】'
        , '添加永久授权* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqtj&key=' . $key . '&qq=$回声段落1$&zrqq=$回声段落2$】%机器人主人要求是(权限要求:机器人主人)%'
        , '查授权* 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqcx&key=' . $key . '&qq=$回声段落1$】'
        , '我的授权 【显示网址内容http://' . $host . '/public/action/api/RobotApi.php?type=sqmy&key=' . $key . '】');
         return $apiArray;
    }
}

?>