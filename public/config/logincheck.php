<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-13 15:49
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

//没有登录
session_start();
//die($_SESSION["sixcloud_token"]);
if (!isset($_SESSION["sixcloud_token"])) {
    die(header("Location:/admin/page/login/"));
}
include("encryption.php");

//登录过，验证是否过期
$token = authcode(sixcloudslashes($_SESSION['sixcloud_token']), 'DECODE', SYSTEM_KEY);
list($user, $sid) = explode("\t", $token);
$session = md5($_SESSION['username'] . $_SESSION['password']);
if ($session != $sid) {
    die(header("Location:/admin/page/login/"));
}

