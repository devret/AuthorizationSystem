<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-12 1:27
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

class KmInfo
{
    public $id;
    public $createuserid;
    public $card;
    public $use;
    public $usedate;
    public $expiredate;
    public $createdate;

}