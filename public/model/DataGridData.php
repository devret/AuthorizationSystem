<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-12 13:01
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

class DataGridData
{
    public $code;
    public $msg;
    public $count;
    public $data;

}