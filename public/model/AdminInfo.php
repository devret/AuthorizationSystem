<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-13 1:13
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

class AdminInfo
{
    public $id;
    public $username;
    public $password;
}