<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-12 12:47
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

class WebConfigInfo
{
    public $id;
    public $headtitle;
    public $foottitle;
    public $music;
    public $contact;
    public $version;
    public $template;

}