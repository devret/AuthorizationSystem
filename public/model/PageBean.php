<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-12 12:53
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

class PageBean
{
    public $page;
    public $size;

    public function __construct($page, $size)
    {
        $this->page = ($page - 1)*$size;
        $this->size = $size;
    }
}