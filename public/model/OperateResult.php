<?php

class OperateResult
{
    public static function out($success, $msg, $object)
    {
        return json_encode(array('success' => $success, 'msg' => $msg, 'data' => $object));
    }

    public static function outArray($success,$msg,$object){
        return array(
            "success"=>$success,
            "msg"=>$msg,
            "data"=>$object);
    }
}
?>