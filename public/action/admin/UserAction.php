<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-15 20:16
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

define('ROOT', str_replace('', '', realpath(dirname(__FILE__) . '/../../../')) . "/");
include(ROOT . "public/service/admin/UserInfoService.php");
if (!isset($_GET['type'])) {
    die(OperateResult::out(false, "type参数不能为空", ""));
}
$type=$_GET['type'];
$UserInfoService = new UserInfoService();
$UserInfo = new AdminInfo();
session_start();
$UserInfo->id=$_SESSION['id'];
$UserInfo->username=$_GET['username'];
$UserInfo->password=$_GET['password'];
switch($type){
    case 'up':
        updateUserInfo($UserInfoService, $UserInfo);
        break;
    default;
        die(OperateResult::out(false, "type参数错误!", ""));
}

/**
 * 修改管理员信息
 * @param $UserInfoService
 * @param $UserInfo
 */
function updateUserInfo($UserInfoService,$UserInfo){
    $isRet = $UserInfoService->AdminInfoUpdateByAdmin($UserInfo);
    die($isRet);
}