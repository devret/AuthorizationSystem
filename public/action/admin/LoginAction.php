<?php
define('ROOT',str_replace('','',realpath(dirname(__FILE__).'/../../../'))."/");
include(ROOT."public/service/admin/LoginService.php"); 

if(!isset($_POST['type'])){
	die(OperateResult::out(false,"参数不能为空",""));
}
$type = $_POST['type'];
$LoginService = new LoginService();
$AdminInfo = new AdminInfo();
switch ($type) {
    case 'login':
        Login($LoginService, $AdminInfo);
        break;
    case 'logout':
        LoginOut($LoginService);
        break;
    default;
        die(OperateResult::out(false, "type参数错误!", ""));
}

/**
 * 管理员登录
 * @param $LoginService
 * @param $AdminInfo 管理员帐号参数
 */
function Login($LoginService,$AdminInfo){
    $AdminInfo->username = $_POST['username'];
    $AdminInfo->password=$_POST['password'];
    $isRet = $LoginService->isLogin($AdminInfo);
    die($isRet);
}

function LoginOut($LoginService){
    $isRet = $LoginService->Logout();
    die($isRet);
}

?>