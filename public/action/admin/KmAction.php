<?php
/**
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-12 1:25
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

define('ROOT', str_replace('', '', realpath(dirname(__FILE__) . '/../../../')) . "/");
include(ROOT . "public/service/admin/KmService.php");

if (!isset($_GET['type'])) {
    die(OperateResult::out(false, "参数不能为空", ""));
}
$type = $_GET['type'];
$KmInfo = new KmInfo();
$KmInfo->id = $_GET['id'];
$KmInfo->createuserid = $_GET['createuserid'];
$KmInfo->expiredate = $_GET['expiredate'];
$CreateNum = $_GET['createnum'];

$KmService = new KmService();
switch ($type) {
    case "add":
        KmCreate($KmService, $KmInfo, $CreateNum);
        break;

    case "del":
        KmDelete($KmService, $KmInfo);
        break;

    case "alldel":
        KmAllDelete($KmService);
        break;

    case "all":
        getAllKm($KmService);
        break;
}

/**
 * 生成卡密
 * @param $KmService
 * @param $KmInfo 要生的卡密信息包含生成人、到期时间
 * @param $CreateNum    需要创建卡密的数量
 */
function KmCreate($KmService, $KmInfo, $CreateNum)
{
    $isRet = $KmService->KmCreateByAdmin($KmInfo, $CreateNum);
    die($isRet);
}

/**
 * 管理员根据卡密id删除卡密
 * @param $KmService
 * @param $KmInfo
 */
function KmDelete($KmService, $KmInfo)
{
    $idArray =json_decode($_GET['id'],true);
   // die(var_dump($idArray));
    $isRet = $KmService->KmDeleteByAdmin($idArray);
    die($isRet);
}

/**
 * 清空所有卡密
 * @param $KmService
 */
function KmAllDelete($KmService)
{
    $isRet = $KmService->KmAllDeleteByAdmin();
    die($isRet);
}

/**
 * 获取全部卡密
 * @param $KmService
 */
function getAllKm($KmService)
{
    $Page = new PageBean($_GET['page'], $_GET['rows']);
    $isRet = $KmService->getAllKmByAdmin($Page);
    die($isRet);
}

?>