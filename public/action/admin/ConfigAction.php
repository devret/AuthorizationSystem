<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-13 0:38
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

define('ROOT', str_replace('', '', realpath(dirname(__FILE__) . '/../../../')) . "/");
include(ROOT . "public/service/admin/ConfigService.php");

if (!isset($_GET['type'])) {
    die(OperateResult::out(false, "type参数不能为空", ""));
}
$type = $_GET['type'];

$ConfigService = new ConfigService();
$WebConfigInfo = new WebConfigInfo();
switch ($type) {
    case 'up':
        ConfigUpdate($ConfigService, $WebConfigInfo);
        break;
    case 'get':
        ConfigGet($ConfigService);
        break;
    case 'getStatistics':
        getStatistics($ConfigService);
        break;
    default;
        die(OperateResult::out(false, "type参数错误!", ""));
}

/**
 * 更新网站信息
 * @param $ConfigService
 * @param $WebConfigInfo  网站配置参数
 */
function ConfigUpdate($ConfigService, $WebConfigInfo)
{
    $WebConfigInfo->headtitle = $_GET['headtitle'];
    $WebConfigInfo->foottitle = $_GET['foottitle'];
    $WebConfigInfo->music = $_GET['music'];
    $WebConfigInfo->contact = $_GET['contact'];
    $WebConfigInfo->template = $_GET['template'];

    $isRet = $ConfigService->ConfigUpdateByAdmin($WebConfigInfo);
    die($isRet);
}

/**
 * 获取网站配置信息
 * @param $ConfigService
 */
function ConfigGet($ConfigService){
    $isRet = $ConfigService->ConfigGetByAdmin();
    die($isRet);
}

/**
 * 获取网站统计信息
 */
function getStatistics($ConfigService){
    $isRet = $ConfigService->getDataInfo();
    die($isRet);
}