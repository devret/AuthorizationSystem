<?php
define('ROOT', str_replace('', '', realpath(dirname(__FILE__) . '/../../../')) . "/");
include(ROOT . "public/service/admin/AuthService.php");

if (!isset($_GET['type'])) {
    die(OperateResult::out(false, "参数不能为空", ""));
}
$type = $_GET['type'];

$AuthService = new AuthService();
$AuthInfo = new AuthInfo();
$AuthInfo->id = $_GET['id'];
$AuthInfo->authorizer = $_GET['authorizer'];
$AuthInfo->auth = $_GET['auth'];
$AuthInfo->authkm = $_GET['authkm'];
$AuthInfo->expiredate = $_GET['expiredate'];
$AuthInfo->status = $_GET['status'];
$AuthInfo->api = $_GET['api'];
$AuthInfo->remark = $_GET['remark'];
//var_dump($AuthInfo);
switch ($type) {
    case 'addnotkm':
        addByNotUseKm($AuthService, $AuthInfo);
        break;
    case 'del':
        authInfoDelete($AuthService, $AuthInfo);
        break;
    case 'up':
        authInfoUpdate($AuthService, $AuthInfo);
        break;
    case "all":
        getAllAuth($AuthService, $AuthInfo);
        break;
    case "alldel":
        authAllDelete($AuthService);
        break;
    case "showapi":
        exportRobotApiText($AuthService);
        break;
    default;
        die(OperateResult::out(false, "type参数不能为空", ""));
}
/**
 * @param $AuthService
 * @param $AuthInfo  授权参数
 */
function addByNotUseKm($AuthService, $AuthInfo)
{
    $isRet = $AuthService->addAuthByAdmin($AuthInfo);
    die($isRet);
}

/**
 * 更新授权信息-id必须传入
 * @param $AuthService
 * @param $AuthInfo 授权参数
 */
function authInfoUpdate($AuthService, $AuthInfo)
{
    $isRet = $AuthService->authInfoUpdateByAdmin($AuthInfo);
    die($isRet);
}

/**
 * 删除授权信息-id必须传入
 * @param $AuthService
 * @param $id 需要删除的id
 */
function authInfoDelete($AuthService, $AuthInfo)
{
    $idArray = json_decode($AuthInfo->id, true);
    $isRet = $AuthService->authInfoDeleteByAdmin($idArray);
    die($isRet);
}

/**
 * 管理员查看授权帐号列表,可选择查询，分页参数必须传入
 * @param $AuthService
 * @param $AuthInfo
 */
function getAllAuth($AuthService, $AuthInfo)
{
    $Page = new PageBean($_GET['page'], $_GET['rows']);
    $isRet = $AuthService->getAllAuthByAdmin($AuthInfo, $Page);
    die($isRet);
}

/**
 * 清空所有授权
 * @param $AuthService
 */
function authAllDelete($AuthService)
{
    $isRet = $AuthService->authAllDeleteByAdmin();
    die($isRet);
}

function exportRobotApiText($AuthService)
{
    $key = $_GET['key'];
    $isRet = $AuthService->exportRobotApiText($key);
    die($isRet);
}

?>