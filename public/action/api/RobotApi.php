<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-19 20:47
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

header('Content-Type: text/html; charset=UTF-8');
$ROOT_PATH = $_SERVER['DOCUMENT_ROOT'];
include($ROOT_PATH . "/public/service/api/RobotApiService.php");
global $robot;
global $sendMsgPerson;//发送消息人的qq帐号

/**
 * 1.如果使用的非晨风机器人调用此接口-只需要改下面二段代码即可
 * 2.获取机器人帐号-兼容不同框架的机器人
 */
 $robot =  $_REQUEST['cfrobotselfnum'];//晨风机器框架获取机器人帐号方法
//$robot =  $_GET['非晨风机器人获取帐号的参数'];//根据你自己所使用的框架更换参数

$sendMsgPerson = $_REQUEST['cfrobotqqnum'];//晨风框架获取发送消息人的qq帐号
//$sendMsgPerson = $_GET['非晨风框架获取发送消息人帐号'];////根据你自己所使用的框架更换参数

/******以下部分不懂无需更改**********/
if (!isset($_GET['type'])) {
    die(OperateResult::out(false, "type参数不能为空", ""));
}

$type = $_GET['type'];

$AuthInfo = new AuthInfo();
$RobotApiService = new RobotApiService();

//权限检测
rootCheak($RobotApiService, $AuthInfo);
switch ($type) {
    case 'sqtj':
        sqtj($RobotApiService, $AuthInfo);
        break;
    case 'sqcx':
        sqcx($RobotApiService, $AuthInfo);
        break;
    case 'sqmy':
        sqmy($RobotApiService, $AuthInfo);
        break;
    case 'sqscmy':
        sqscmy($RobotApiService, $AuthInfo);
        break;
    case 'sqsc':
        sqsc($RobotApiService, $AuthInfo);
        break;
    case 'sqghmy':
        sqghmy($RobotApiService, $AuthInfo);
        break;
    case 'sqgh':
        sqgh($RobotApiService, $AuthInfo);
        break;
    case 'sqghzr':
        sqghzr($RobotApiService, $AuthInfo);
        break;
    case 'sqclear': //清除某主人下的所有帐号
        sqclear($RobotApiService, $AuthInfo);
        break;
    case 'getkey'://获取卡密
        getKey($RobotApiService, $AuthInfo);
        break;
    case 'kmadda'://生成永久卡密
        addLastingKm($RobotApiService);
        break;
    case 'kmaddb'://生成期限卡密
        addTimeKm($RobotApiService);
        break;
    case 'sqdj'://授权冻结
        sqStop($RobotApiService, $AuthInfo);
        break;
    case 'sqjh'://授权冻结
        sqActivation($RobotApiService, $AuthInfo);
        break;
}

//权限检测
function rootCheak($RobotApiService, $AuthInfo)
{
    $robot = $GLOBALS['robot'];
    $AuthInfo->key = $_GET['key'];//接口机器人key
    $isArr = $RobotApiService->chenkApi($robot, $AuthInfo->key);
    if ($isArr['success'] == false) {
        die($isArr['msg']);
    }
}

/**
 * 机器人通用输出
 * @param $AuthInfo
 * @param $retArr
 */
function robotOut($qq, $retArr)
{
    if ($retArr['success'] != true) {
        die("【换行】操作对象:" . $qq . "【换行】操作结果:【换行】" . $retArr['msg']);
    } else {
        die("【换行】操作对象:" .$qq . "【换行】操作结果:【换行】" . $retArr['msg']);
    }
}

/**
 * 查询某个号是否授权
 * @param $RobotApiService
 * @param $AuthInfo
 */
function sqcx($RobotApiService, $AuthInfo)
{
    $qq = $_GET['qq'];
    if ($qq == "") {
        die("查授权 QQ");
    }
    $retArr = $RobotApiService->findSqByAuth($qq);

    if ($retArr['success'] == false) {
        die("授权状态：未授权【换行】查询帐号：" . $qq . "【换行】当前时间：" . NOW_DATE);
    } else if($retArr['data']['expiredate']!=1){
        die("授权状态：已授权【换行】授权帐号：" . $qq . "【换行】授权主人：" . $retArr['data']['authorizer'] . "【换行】授权时间：" . $retArr['data']['createdate']."【换行】到期时间：".$retArr['data']['expiredate']);
    }else{
		die("授权状态：已授权【换行】授权帐号：" . $qq . "【换行】授权主人：" . $retArr['data']['authorizer'] . "【换行】授权时间：" . $retArr['data']['createdate']."【换行】到期时间：永久授权");
	}
}

/**
 * 授权添加
 * @param $RobotApiService
 * @param $AuthInfo
 */
function sqtj($RobotApiService, $AuthInfo)
{

    //权限验证通过后检测需要授权的帐号是否存在授权
    $AuthInfo->authorizer = $_GET['zrqq'];
    $AuthInfo->auth = $_GET['qq'];
    if ($AuthInfo->authorizer == "" || $AuthInfo->auth == "") {
        die("添加永久授权 QQ 主人QQ");
    }
    $retArr = $RobotApiService->findSqByAuth($AuthInfo->auth);
    if ($retArr['success'] == true) {
        die("该帐号已经存在授权！");
    }

    //需要授权的帐号不存在授权-添加新授权
    $retArr = $RobotApiService->sqtj($AuthInfo);
    if ($retArr['success'] != true) {
        die("授权状态：失败【换行】授权帐号：" . $AuthInfo->auth . "【换行】授权主人：" . $AuthInfo->authorizer . "【换行】当前时间：" . NOW_DATE);
    } else {
        die("授权状态：成功【换行】授权帐号：" . $AuthInfo->auth . "【换行】授权主人：" . $AuthInfo->authorizer . "【换行】当前时间：" . NOW_DATE);
    }
}

/**
 * @param $RobotApiService
 * @param $AuthInfo查询授权类别 -通过主人
 */
function sqmy($RobotApiService, $AuthInfo)
{
    $zrqq = $GLOBALS['sendMsgPerson'];
    if ($zrqq == "") {
        die("请使用晨风机器人调用");
    }
    $retArr = $RobotApiService->findSqByZrQQ($zrqq);

    if ($retArr['success'] == false) {
        die("未查询到授权信息");
    } else {
        $str = "";
        //  die(json_encode([0]['auth']));
        for ($i = 0; $i < count($retArr['data']); $i++) {
            $str .= $retArr['data'][$i]['auth'] . "【换行】";
        }
        die("查询帐号：" . $zrqq . "【换行】授权信息如下:【换行】" . $str);
    }
}


/**
 *机器人主人删除自己的授权帐号
 */
function sqscmy($RobotApiService, $AuthInfo)
{
    $AuthInfo->authorizer = $GLOBALS['sendMsgPerson'];
    $AuthInfo->auth = $_GET['qq'];
    if ($AuthInfo->authorizer == "" || $AuthInfo->auth == "") {
        die("删除我的授权  QQ");
    }

    $retArr = $RobotApiService->deleteSqByZrqqAndQQ($AuthInfo);
    //返回操作信息
    robotOut($AuthInfo->auth, $retArr);
}

/**
 *删除任意授权帐号 - 一般权限是管理员
 */
function sqsc($RobotApiService, $AuthInfo)
{
    $qq = $_GET['qq'];
    if ($qq == "") {
        die("删除授权 QQ");
    }

    $AuthInfo->auth = $qq;

    $retArr = $RobotApiService->deleteSqBySqQQ($AuthInfo);
    //返回操作信息
    robotOut($qq, $retArr);
}

/**
 * 机器人主人更换自己的授权帐号
 */
function sqghmy($RobotApiService, $AuthInfo)
{
    $AuthInfo->authorizer = $GLOBALS['sendMsgPerson'];
    $AuthInfo->auth = $_GET['qq'];
    $NewQQ = $_GET['newqq'];
    if ($AuthInfo->authorizer == "" || $AuthInfo->auth == "" || $NewQQ == "") {
        die("更换我的授权 原QQ 新QQ");
    }

    $retArr = $RobotApiService->updateSqByZrQQAndQQ($AuthInfo, $NewQQ);
    //返回操作信息
    robotOut($AuthInfo->auth, $retArr);
}

/**
 * 更换授权 - 一般权限要求是管理员
 */
function sqgh($RobotApiService, $AuthInfo)
{
    $AuthInfo->auth = $_GET['qq'];
    $NewQQ = $_GET['newqq'];
    if ($AuthInfo->auth == "" || $NewQQ == "") {
        die("更换授权 旧QQ 新QQ");
    }

    $retArr = $RobotApiService->updateSqBySqQQ($AuthInfo, $NewQQ);
    //返回操作信息
    robotOut($AuthInfo->auth, $retArr);
}

/**
 * 授权信息更换主人
 */
function sqghzr($RobotApiService, $AuthInfo)
{
    $AuthInfo->auth = $_GET['qq'];
    $NewZrQQ = $_GET['newzrqq'];
    if ($AuthInfo->auth == "" || $NewZrQQ == "") {
        die("更换主人 机器人QQ 主人QQ");
    }

    $retArr = $RobotApiService->updateSqZrQQ($AuthInfo, $NewZrQQ);
    //返回操作信息
    robotOut($AuthInfo->auth, $retArr);
}

/**
 * 删除某帐号下的所有授权
 * @param $RobotApiService
 * @param $AuthInfo
 */
function sqclear($RobotApiService, $AuthInfo)
{
    $AuthInfo->authorizer = $_GET['zrqq'];
    if ($AuthInfo->authorizer == "") {
        die("清空授权 主人QQ");
    }
    $retArr = $RobotApiService->deleteAllSqByZr($AuthInfo);
    robotOut($AuthInfo->authorizer, $retArr);
}

/**
 * 获取机器人key
 * @param $RobotApiService
 * @param $AuthInfo
 */
function getKey($RobotApiService, $AuthInfo)
{
    $AuthInfo->auth = $_GET['qq'];
    $AuthInfo->authorizer = $GLOBALS['sendMsgPerson'];
    if ($AuthInfo->auth == "") {
        die("获取key 机器人QQ");
    }
    $retArr = $RobotApiService->getKey($AuthInfo);
    if ($retArr['success'] != true) {
        die("操作账户:" . $AuthInfo->authorizer . "【换行】操作对象:" . $AuthInfo->auth . "【换行】操作结果:【换行】" . $retArr['msg']);
    } else {
        die('已将key私聊给你！$发消息私聊 ' . $AuthInfo->auth . '的key是:【换行】' . $retArr['data']['key'] . '隐藏$');
    }
}

/**
 * 添加永久卡密
 */
function addLastingKm($RobotApiService)
{
    $num = $_GET['num'];
    if ($num == "") {
        die("生成永久卡密 数量");
    }

    $retArr = $RobotApiService->addLastingKm($num);
    if ($retArr['success'] == false) {
        die("卡密生成失败！");
    }
    $km = "永久卡密【换行】";
    //die(var_dump($retArr['data']));
    foreach ($retArr['data'] as $a) {
        $km .= $a[1][1] . '【换行】';
    }
    die('已将卡密私聊给你！$发消息私聊 ' . $km . '隐藏$');
}

/**
 * 生成期限卡密
 * @param $RobotApiService
 */
function addTimeKm($RobotApiService)
{
    $num = $_GET['num'];
    $day = $_GET['day'];
    if ($num == "" || $day == "" || $day == 0) {
        die("生成期限卡密 数量 天数");
    }
    $retArr = $RobotApiService->addTimeKm($num, $day);
    if ($retArr['success'] == false) {
        die("卡密生成失败！");
    }
    $km = $day . "天卡密【换行】";
    //die(var_dump($retArr['data']));
    foreach ($retArr['data'] as $a) {
        $km .= $a[1][1] . '【换行】';
    }
    die('已将卡密私聊给你！$发消息私聊 ' . $km . '隐藏$');
}

/**
 * 冻结授权
 * @param $RobotApiService
 * @param $AuthInfo
 */
function sqStop($RobotApiService, $AuthInfo)
{
    $AuthInfo->auth = $_GET['qq'];

    if ($AuthInfo->auth == "") {
        die("授权冻结 机器人QQ");
    }
    $retArr = $RobotApiService->sqStop($AuthInfo);

    robotOut($AuthInfo->auth, $retArr);
}

/**
 * 授权激活
 * @param $RobotApiService
 * @param $AuthInfo
 */
function sqActivation($RobotApiService, $AuthInfo)
{
    $AuthInfo->auth = $_GET['qq'];
    if ($AuthInfo->auth == "") {
        die("授权激活 机器人QQ");
    }
    $retArr = $RobotApiService->sqActivation($AuthInfo);

    robotOut($AuthInfo->auth, $retArr);
}

?>
