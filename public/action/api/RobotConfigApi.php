<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-03-16 0:43
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

header('Content-Type: text/html; charset=UTF-8');
$ROOT_PATH = $_SERVER['DOCUMENT_ROOT'];
include($ROOT_PATH . "/public/service/api/RobotApiService.php");
$AuthInfo = new AuthInfo();
$RobotApiService = new RobotApiService();

if (!isset($_REQUEST['cfrobotselfnum'])) {
    die("请使用晨风机器人调用");
}
$jqrQQ = $_REQUEST['cfrobotselfnum'];
$retArr = $RobotApiService->findSqByAuth($jqrQQ);

//以下需要修改的请根据实际业务逻辑编写
if ($retArr['success'] == false) {
    die("未授权");
} else if ($retArr['data']['status'] != 1) {
    die("授权已冻结");
} else if($retArr['data']['expiredate'] !=1 && $retArr['data']['expiredate']< NOW_DATE){
	 die("授权已到期");
}else{
    die("已授权");
}