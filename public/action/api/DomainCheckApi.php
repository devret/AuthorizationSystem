<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-03-16 0:43
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 *
在你的代刷网之类的核心php文件的末尾添加以下代码即可实现授权验证
 // 需要添加的代码开始
if(!isset($_SESSION['authcode']))
{
$query=file_get_contents('http://你的域名/public/action/api/DomainCheckApi.php?url='.$_SERVER['HTTP_HOST'].'&authcode='.$authcode);
if($query=json_decode($query,true)) {
if($query['code']==1){
$_SESSION['authcode']=true;
}else {
exit(''.$query['msg'].'');
}
}
}
 // 需要添加的代码结束
 */

header('Content-Type: text/html; charset=UTF-8');
$ROOT_PATH = $_SERVER['DOCUMENT_ROOT'];
include($ROOT_PATH . "/public/service/api/RobotApiService.php");

$RobotApiService = new RobotApiService();

$url = $_GET['url'];

$retArr = $RobotApiService->findSqByAuth($url);
//以下需要修改的请根据实际业务逻辑编写
if ($retArr['success'] == false) {
    die(json_encode(array("code"=>0,"msg"=>"网站还未授权")));
} else if ($retArr['data']['status'] != 1) {
    die(json_encode(array("code"=>0,"msg"=>"网站授权已被禁用")));
} else if ($retArr['data']['expiredate'] != 1 && $retArr['data']['expiredate'] < NOW_DATE) {
	  die(json_encode(array("code"=>0,"msg"=>"网站授权已到期")));
}else{
	 die(json_encode(array("code"=>1,"msg"=>"正版授权")));
}

?>