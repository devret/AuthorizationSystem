<?php
/**
晨风机器人软件破解授权验证,获取授权帐号信息方法
此文件只适用 6云授权系统
特点:简化数据查询,不用特意拼接查询字符串
数据库操作类详细说明:https://blog.csdn.net/qq_40622375/article/details/89340137
你的授权验证文件可以放在网站目录的任何位置,只需修改相关信息即可
 */
//导入数据库操作类
include($_SERVER['DOCUMENT_ROOT'].'/'.'public/dao/db.class.php');


//获取破解版晨风机器人传入的QQ
$qqnum=$_REQUEST['version'];

//破解算法解析接口(替换成你自己的算法接口)-只需要修改url地址即可
$url='http://sixcloud.co/xxx/xxxx.php?qq='.$qqnum;

/**************以下不懂无需修改********************/
//查询帐号授权语句
//$sql ="SELECT * FROM `sixcloud_infos` WHERE `auth` =?";

//如果想实现账户虽然授权了,但是系统里面是被禁用掉的,限制使用，可以使用下面这个语句
$sql ="SELECT * FROM `sixcloud_infos` WHERE `auth` =? and `status` = 1";

//执行查询数据库 $row就是查询出来的数据,是二维数组,具体看上面的地址链接
$row = SQL::Read($sql ,array($qqnum));

//取消前面的注释可以查询读取出来的数据
//die(var_dump($row));

//如果没有授权就回复错误码
if(!$row){die('20_f403bb1e45745f6a4b8672054ea05dd5');}
/**************以上无需修改********************/


$result=file_get_contents($url);
//这里输出你接口地址返回的内容
echo  $result;
?>