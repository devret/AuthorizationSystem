<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-03-16 10:17
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

header('Content-Type: text/html; charset=UTF-8');
$ROOT_PATH = $_SERVER['DOCUMENT_ROOT'] . '/';
include($ROOT_PATH . "public/service/api/UseCountService.php");
$UseCountService = new UseCountService();
$ret = $UseCountService->getApiUseCount();
if(isset($_GET['type']) && $_GET['type']=='usecount'){
    echo $ret['data']['usecount'];
}else{
    define("USECOUNT",$ret['data']['usecount']) ;
}