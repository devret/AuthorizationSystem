<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-06-27 17:21
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 * 此文件返回授权信息的json数据,方便与其它程序进行对接。
 * 注意：需要先修改安全码($passCode)才可以正常使用。
 * 调用方式：域名/public/action/api/json.php?safecode=安全码&query=查询帐号
 */

header('Content-Type: application/json; charset=UTF-8');
$ROOT_PATH = $_SERVER['DOCUMENT_ROOT'];
include($ROOT_PATH . "/public/service/api/RobotApiService.php");


//传入的安全码
$safeCOde = $_GET['safecode'];
//需要匹配的安全码(修改成你自己的安全码)
$passCode = 666;
//验证安全码
if($safeCOde=="" || $safeCOde!=$passCode){
	die(OperateResult::out(false, "安全码不正确或未修改,请检查/public/action/api/json.php文件是否设置正确", null));
}
$RobotApiService = new RobotApiService();
$type=$_GET['type'];

switch($type){
	//授权信息查询
	case 'query':sqcx($RobotApiService);break;
	//生成永久卡密
	case 'kmadda':addLastingKm($RobotApiService);break;
	//生成期限卡密
	case 'kmaddb':addTimeKm($RobotApiService);break;
    case 'kmquery':kmcx($RobotApiService);break;
	default:die(OperateResult::out(false, "type参数错误", null));
}

/**
 * 查询某个号是否授权
 * @param $RobotApiService
 */
function sqcx($RobotApiService){
	$qq = $_GET['search'];

	//查询
	$retArr = $RobotApiService->findSqByAuth($qq);

	echo json_encode($retArr);
}

/**
 * 添加永久卡密
 */
function addLastingKm($RobotApiService)
{
    $num = $_GET['num'];
    if ($num == "") {
		
        die(OperateResult::out(false, "参数num(生成数量)不能为空", null));
    }

    $retArr = $RobotApiService->addLastingKm($num);
	if($retArr['success']!=false){
		$kmArr = array();
		foreach ($retArr['data'] as $a) {
		   array_push($kmArr,$a[1][1]);
		}
			$retArr['data'] = $kmArr;
	}
    echo json_encode($retArr);
}

/**
 * 生成期限卡密
 * @param $RobotApiService
 */
function addTimeKm($RobotApiService)
{
    $num = $_GET['num'];
    $day = $_GET['day'];
    if ($num == "" || $day == "" || $day == 0) {
		 die(OperateResult::out(false, "参数num(生成数量)、参数day(卡密天数)不能为空", null));
    }
    $retArr = $RobotApiService->addTimeKm($num, $day);
	
   if($retArr['success']!=false){
		$kmArr = array();
		foreach ($retArr['data'] as $a) {
		   array_push($kmArr,$a[1][1]);
		}
			$retArr['data'] = $kmArr;
	}
    echo json_encode($retArr);
}

/**
 * 卡密查询
 * @param $RobotApiService
 */
function kmcx($RobotApiService){
    $km = $_GET['km'];
    if ($km == "") {
        die(OperateResult::out(false, "参数km(查询的卡密)不能为空", null));
    }

    $KmInfo  = new KmInfo();
    $KmInfo->card=$km;
    $retArr = $RobotApiService->findKmInfo($KmInfo);
       echo json_encode($retArr);
}

?>