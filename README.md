Six云网络授权系统
===============
## 版本信息
* 当前版本 V1.5
* [完整包下载](http://version.916b.cn/release/release1.5.zip)
* [更新包下载](http://version.916b.cn/update/update1.5.zip)
* [接口文档](http://version.916b.cn/doc/api.html)
* [查看更新日志](https://gitee.com/devret/AuthorizationSystem/blob/master/UPDATE.md)
## 项目介绍
 * 最简洁、清爽、易用的网络授权系统。

 * 此项目采用前后端分离开发，前端采用layui框架，后端采用原生PHP开发。

 * 项目会不定时进行更新，建议star和watch一份。

 * 交流QQ群：[1031929745](点击链接加入群聊【云授权系统】：https://jq.qq.com/?_wv=1027&k=5J3SSgO) `加群请备注来源：如gitee、github、官网等`。

## 代码仓库
 * 在线预览地址：[http://sq.sixcloud.co](http://sq.sixcloud.co)
 * Gitee仓库地址：[https://gitee.com/devret/AuthorizationSystem.git](https://gitee.com/devret/AuthorizationSystem.git)
 * GitHub仓库地址：[https://github.com/wyx176/AuthorizationSystem.git](https://github.com/wyx176/AuthorizationSystem.git)

## 主要特性
* 界面足够简洁清爽，响应式且适配手机端。
* 页面支持多配色方案，可自行选择喜欢的配色。
* 支持多tab，可以打开多窗口。
* 刷新页面会保留当前的窗口，并且会定位当前窗口对应左侧菜单栏。
* 首页采用可视化数据统计，数据状态一目了然。
* 一键修改前台模板页信息，方便快捷。
* 后台管理支持切换前台用户模板，随时换“装”，主页不在单调。
* 可随时增加自己喜欢的模板，只需要几行代码即可调用网站信息配置前端模板。

## 效果预览
> 总体预览

![Image text](./images/home.png)

## 环境要求
 * PHP版本：5.4+

## 目录结构
----admin 后台管理系统前端页面<br>
    ---------api 侧边栏菜单定义<br>
       ---------page 页面模板<br>
        ----index 存放网站主页模板、一个模板一个文件夹<br>
        ----install 安装程序路径<br>
        ----public 后端文件存放目录<br>
        ---------action 接口php存放位置,前端所有请求都在action文件夹中<br>
        ------------------admin 后台管理系统接口<br>
        ------------------api 前台用户页、机器人请求接口均存放在此<br>
        ---------config 项目配置文件 一般不需要修改<br>
        ---------dao 数据库操作目录<br>
        ---------model 实体层<br>
        ---------service 业务逻辑层 修改业务逻辑一般都需要在这里操作<br>
        ---------utils 工具类<br>

## 使用说明

* 文档地址：源代码中的"授权系统接口文档.html"
* 注意：如需要进去二次开发建议把文件建立在对应文件夹中
* 机器人请求接口存放在public/action/api/RobotAction.php
* 对应的业务逻辑代码存放在public/service/api/RobotService.php

## 如何添加前台模板?
* 1.在index/文件夹下建立一个新的文件夹(名字不要太长)。
* 2.把你的页面模板放在建立的文件中即可，然后去后台管理系统切换模板即可。
* 3.如何配置模板上显示网站信息?<br>
    你只需要在你模板文件的头部开启session,然后通过`$_SESSION['config']`即可获取到前台数据信息。

```
$_SESSION['config'] 前台网站信息数组
$_SESSION['config']['headtitle'] 网站头部信息
$_SESSION['config']['contact'] 网站联系人QQ
$_SESSION['config']['foottitle'] 网站底部信息<
$_SESSION['config']['music'] 网站音乐链接
```

 ## 捐赠支持
 
开源项目不易，若此项目能得到你的青睐，可以捐赠支持作者持续开发与维护，感谢所有支持开源的朋友。

 ![Image text](./images/pay.jpg)
