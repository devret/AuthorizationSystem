<?php
session_start();
if (!isset($_SESSION['config'])) {
    header("Location:/");
}
?>
<DOCTYPE HTML>
	<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title><?php echo $_SESSION['config']['headtitle']; ?>授权系统</title>
		<meta name="description" content="授权系统-6云互联-专业虚拟主机供应商" />
		<meta name="keywords" content="晨风机器人|插件|配置|接口|授权系统|机器人论坛|6云互联" />
		<link rel="shortcut icon"  type="image/x-icon" href="img/favicon.ico" />
		<link rel="stylesheet" href="css/main.css" />

		<style>
			.momo {
				font-style: normal;
				font-weight: normal;
				speak: none;
				display: inline-block;
				text-decoration: inherit;
				width: 1em;
				margin-right: .2em;
				text-align: center;
				font-variant: normal;
				text-transform: none;
				line-height: 1em;
				margin-left: .2em;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
				-webkit-animation: xintiao 1.33s ease-in-out infinite;
				animation: xintiao 1.33s ease-in-out infinite
			}
			
			@-webkit-keyframes xintiao {
				0% {
					-webkit-transform: scale(1)
				}
				50% {
					-webkit-transform: scale(.8)
				}
				to {
					-webkit-transform: scale(1)
				}
			}
			
			@keyframes xintiao {
				0% {
					-webkit-transform: scale(1);
					transform: scale(1)
				}
				50% {
					-webkit-transform: scale(.8);
					transform: scale(.8)
				}
				to {
					-webkit-transform: scale(1);
					transform: scale(1)
				}
			}
		</style>
		<style>
			/** 梅花枝 **/
			
			.meiha {
				position: fixed;
				top: 0;
				right: 0;
				z-index: 0;
				width: 350px;
				/** PNG图宽度 **/
				height: 231px;
				/** PNG图高度 **/
				pointer-events: none;
				background: url(img/1kBpid.png);
			}
		</style>

	</head>

	<body class="is-loading">
		<div id="wrapper">
			<section id="main">
				<header>
					<span class="avatar">
						<img src="http://q.qlogo.cn/headimg_dl?bs=qq&dst_uin=<?php echo $_SESSION['config']['contact']; ?>&src_uin=sixcloud.co&fid=blog&spec=100" class="logo">
					</span>
					<a href="#">
						<h1><?php echo $_SESSION['config']['headtitle']; ?>授权系统</h1></a>
					<p>欢迎来到<?php echo $_SESSION['config']['headtitle']; ?>科技权官网</p></a>
				

				</header>

				<div class="">
					<ul class="icons">
						<li>
							<a href="sq.php">开始授权</a>
						</li>
						<li>
							<a href="sqcx.php">查询授权</a>
						</li>
						<li>
							<a target="_target" href="https://sixcloud.co">购买卡密</a>
						</li>
						<li>
							 <a target="_target" href="https://wpa.qq.com/msgrd?v=3&uin=<?php echo $_SESSION['config']['contact']; ?>&site=qq&menu=yes">联系在线客服</a>
						</li>
						<li>
							<a target="_target" href="/admin">后台登录</a>
						</li>
						<li>
							<a target="_target" href="https://sixcloud.co/">6云互联</a>
						</li>
					</ul>

				</div>

			</section>
			<div class="meiha"></div>
			<footer id="footer">
				<ul class="copyright">

					<li>
						<a href="#"><?php echo $_SESSION['config']['headtitle']; ?>科技权官网</a> |
						 <p class="copyright"><?php echo $_SESSION['config']['foottitle']; ?></p>
				</ul>
			</footer>
			<script>
				if('addEventListener' in window) {
					window.addEventListener('load', function() {
						document.body.className = document.body.className.replace(/\bis-loading\b/, '')
					});
					document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '')
				}
			</script>
			<!--树叶飘落特效-->
			<script src="js/jquery.min.js"></script>
			<script src="js/su.js"></script>
			<script src="js/snowfall.js"></script>
			<!--播放音乐-->
			<audio id="mom" src="<?php echo $_SESSION['config']['music']; ?>" autoplay loop="-1" preload="auto" type="audio/mp3"></audio>
	</body>

	</html>