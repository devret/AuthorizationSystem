<?php
session_start();
if (!isset($_SESSION['config'])) {
    header("Location:/");
}
?>
<!DOCTYPE HTML>
<html>

	<head>
		<title><?php echo $_SESSION['config']['headtitle']; ?>授权系统</title>
		<meta name="keywords" content="晨风机器人|插件|配置|接口|授权系统|机器人论坛|6云互联">
		<meta name="description" content="授权系统-6云互联-专业虚拟主机供应商">
		<link rel="shortcut icon" href="favicon.ico">
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="./css/main.css" />
		<noscript><link rel="stylesheet" href="./css/noscript.css" /></noscript>
	</head>

	<body onselectstart="return false" oncontextmenu=self.event.returnValue=false>

		<!-- Wrapper -->
		<div id="wrapper">

			<!-- Header -->
			<header id="header">
				<div class="logo">
					<img src="http://q.qlogo.cn/headimg_dl?bs=qq&dst_uin=<?php echo $_SESSION['config']['contact']; ?>&src_uin=sixcloud.co&fid=blog&spec=100" class="logo">
				</div>
				<div class="content">
					<div class="inner">
		                <h1><?php echo $_SESSION['config']['headtitle']; ?>授权系统</h1>
		                <p>欢迎来到<?php echo $_SESSION['config']['headtitle']; ?>科技权官网</p>
					</div>
				</div>
				<nav>
					 <ul>
		                <li><a href="sq.php">开始授权</a></li>
		                <li><a href="sqcx.php">查询授权</a></li>
		                <li><a href="https://sixcloud.co">购买卡密</a></li>
		                <li>
		                    <a href="https://wpa.qq.com/msgrd?v=3&uin=<?php echo $_SESSION['config']['contact']; ?>&site=qq&menu=yes">联系在线客服</a>
		                </li>
		                <li><a target="_target" href="/admin">后台登录</a></li>
		                <li><a target="_target" href="https://sixcloud.co/">6云互联</a></li>
		            </ul>
				</nav>
			</header>

			<!-- Footer -->
			<footer id="footer">
				  <p class="copyright"><?php echo $_SESSION['config']['foottitle']; ?></p>
			</footer>
		</div>
		
		<!--bg-->
		<div id="bg"></div>

		<!-- Scripts -->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/util.js"></script>
		<script src="js/main.js"></script>
		<script src="js/su.js"></script>
		<script type="text/javascript">
			$(function(){
				$('body').wpSuperSnow({
					flakes: ['img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/8.png', 'img/7.png', 'img/6.png', 'img/4.png', 'img/2.png', 'img/3.png', 'img/2.png'],
					totalFlakes: '90',
					zIndex: '999999',
					maxSize: '30',
					maxDuration: '20',
					useFlakeTrans: false
				});
			});
		</script>
		<audio id="mom" src="<?php echo $_SESSION['config']['music']; ?>" autoplay loop="-1" preload="auto" type="audio/mp3"></audio>
	</body>
</html>