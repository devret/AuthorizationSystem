<?php
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>授权系统</title>
    <link rel="stylesheet" href="assets/css/auth.css">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/layer/layer.js"></script>
</head>

<body>
<div class="lowin lowin-blue">
    <div class="lowin-brand">
        <img src="http://q.qlogo.cn/headimg_dl?bs=qq&dst_uin=<?php session_start();
        echo $_SESSION['config']['contact']; ?>&src_uin=sixcloud.co&fid=blog&spec=100"
             class="logo">
    </div>
    <div class="lowin-wrapper">


        <div class="lowin-box lowin-register">
            <div class="lowin-box-inner">
                <form>
                    <p>授权系统</p>

                    <div class="lowin-group">

                        <input type="text" autocomplete="jqr" name="jqr" class="lowin-input" placeholder="请输入机器人QQ">
                    </div>
                    <button id="sure" type="button" class="lowin-btn">
                        查询
                    </button>

                    <div class="text-foot">
                        <div>
                            <a style="margin-right: 50%;" href="/" class="login-link">返回</a>
                            <a href="https://wpa.qq.com/msgrd?v=3&uin=<?php echo $_SESSION['config']['contact']; ?>&site=qq&menu=yes"
                               class="login-link">联系我们</a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <footer class="lowin-footer">
        Design By <a href="http://fb.me/itskodinger">@itskodinger</a>
    </footer>
</div>

<script>
    ;!function () {

//页面一打开就执行，放入ready是为了layer所需配件（css、扩展模块）加载完毕
        layer.ready(function () {

        });

        $('#sure').on('click', function () {
            var jqr = $("input[name='jqr']").val();


            if (jqr == "") {
                layer.msg("机器人QQ不能为空！", {icon: 5});
                return false;
            }

            var params = {};
            params.type = "query";
            params.jqr = jqr;

            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            $.ajax({
                url: '../../../public/action/api/SqAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: params,
                success: function (res) {
                    layer.close(index);
                    console.log(res);
                    if (res.success != true) {
                        layer.msg(res.msg, {icon: 5});
                    } else {
                        layer.msg(res.msg, {icon: 6});
                    }
                },
                error: function (data) {
                    layer.close(index);
                    layer.msg(加载系统错误, {icon: 5});
                }
            });//ajax结束
        });

    }();
</script>
</body>
</html>