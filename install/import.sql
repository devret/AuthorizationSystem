/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : newsqxt

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 03/10/2020 09:58:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sixcloud_admin
-- ----------------------------
DROP TABLE IF EXISTS `sixcloud_admin`;
CREATE TABLE `sixcloud_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`, `username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sixcloud_admin
-- ----------------------------
INSERT INTO `sixcloud_admin` VALUES (1, 'admin', 'c3284d0f94606de1fd2af172aba15bf3');

-- ----------------------------
-- Table structure for sixcloud_config
-- ----------------------------
DROP TABLE IF EXISTS `sixcloud_config`;
CREATE TABLE `sixcloud_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headtitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头部标题',
  `foottitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '底部标题',
  `music` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '音乐链接',
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `usecount` int(11) NULL DEFAULT NULL COMMENT '接口调用次数',
  `template` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板名字',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sixcloud_config
-- ----------------------------
INSERT INTO `sixcloud_config` VALUES (1, 'Six云', '<a href=\"https://sixcloud.co/ \">Copyright © 2019 sixcloud.co 6云科技. All rights reserved.</a>', 'http://mp.111ttt.cn/mp3music/6836375.mp3', '461313128', 0, 'v3');

-- ----------------------------
-- Table structure for sixcloud_infos
-- ----------------------------
DROP TABLE IF EXISTS `sixcloud_infos`;
CREATE TABLE `sixcloud_infos`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authkm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '若使用卡密授权则有卡密',
  `authorizer` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权人',
  `auth` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '需要授权的机器人号',
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'key',
  `expiredate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '到期时间-1默认永久',
  `createdate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `status` int(2) NULL DEFAULT 1 COMMENT '授权状态1开通、0禁用',
  `api` int(2) NULL DEFAULT 0 COMMENT '1开通，0无是否具有api功能-通过接口调用生成卡密、授权等等',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`, `auth`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sixcloud_infos
-- ----------------------------

-- ----------------------------
-- Table structure for sixcloud_kms
-- ----------------------------
DROP TABLE IF EXISTS `sixcloud_kms`;
CREATE TABLE `sixcloud_kms`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createuserid` int(11) NULL DEFAULT NULL COMMENT '创建人',
  `card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '卡密',
  `use` int(255) NULL DEFAULT 0 COMMENT '0未使用',
  `usedate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用时间',
  `expiredate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '到期时间-1默认永久',
  `createdate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sixcloud_kms
-- ----------------------------

-- ----------------------------
-- Table structure for sixcloud_version
-- ----------------------------
DROP TABLE IF EXISTS `sixcloud_version`;
CREATE TABLE `sixcloud_version`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统版本',
  `check` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '检测新版本地址',
  `download` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sixcloud_version
-- ----------------------------
INSERT INTO `sixcloud_version` VALUES (1, '1', '1', '1');

SET FOREIGN_KEY_CHECKS = 1;
