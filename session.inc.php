<?php
header('Content-Type: text/html; charset=UTF-8');
define('CACHE_FILE', 0);
define('IN_CRONLITE', true);
define('ROOT_PATH', dirname(__FILE__) . '/');
define('PUBLIC_ROOT', '/public/');
date_default_timezone_set('PRC');
define('NOW_DATE', date('Y-m-d H:i:s'));
include(ROOT_PATH . PUBLIC_ROOT . "dao/db.class.php");
include(ROOT_PATH . PUBLIC_ROOT . "model/OperateResult.php");
include(ROOT_PATH . PUBLIC_ROOT . "utils/Utils.php");
include(ROOT_PATH . PUBLIC_ROOT . "config/logincheck.php");
?>