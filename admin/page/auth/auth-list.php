<?php
include("../../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        <div class="layui-collapse" lay-filter="filter">
            <div class="layui-colla-item">
                <h2 class="layui-colla-title">条件筛选</h2>
                <div class="layui-colla-content">
                    <div style="margin: 10px 10px 10px 10px">
                        <form class="layui-form " action="">

                            <div class="layui-form-item">

                                <div class="layui-inline">

                                    <input type="text" name="authorizer" autocomplete="off" placeholder="联系人"
                                           class="layui-input">

                                </div>
                                <div class="layui-inline">

                                    <input type="text" name="auth" autocomplete="off" placeholder="授权帐号"
                                           class="layui-input">

                                </div>
                                <div class="layui-inline">

                                    <input type="text" name="authkm" autocomplete="off" placeholder="授权卡密"
                                           class="layui-input">

                                </div>
                                <div class="layui-inline">

                                    <select name="status">
                                        <option value="">请选择授权状态</option>
                                        <option value="0">禁用</option>
                                        <option value="1">开通</option>
                                    </select>

                                </div>
                                <div class="layui-inline">
                                    <button type="submit" class="layui-btn layui-btn-primary" lay-submit
                                            lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm data-edit-btn">编辑</button>
                <button class="layui-btn layui-btn-sm layui-bg-orange data-delete-btn">删除</button>
                <button class="layui-btn layui-btn-sm layui-btn-danger data-deleteAll-btn ">X</button>
            </div>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

    </div>
</div>
<script type="text/html" id="currentTableBar">
    <a class="layui-btn layui-btn-sm " lay-event="showRobotApiText" >展示接口文本</a>
</script>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['element', 'form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            element = layui.element;


        //监听折叠
        element.on('collapse(filter)', function (data) {

        });

        table.render({
            elem: '#currentTableId',
            url: '../../../public/action/admin/AuthAction.php',
            where: {type: 'all'},
            request: {
                pageName: 'page' //页码的参数名称，默认：page
                , limitName: 'rows' //每页数据量的参数名，默认：limit
            },
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports'],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 10,
            page: true,
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID', sort: true, hide: true},
                {field: 'auth', width: 110, title: '授权帐号'},
                {field: 'authorizer', width: 110, title: '主人', sort: true},
                {field: 'key', width: 220, title: 'KEY'},
                {field: 'createdate', title: '添加时间', width: 160,},
                {
                    field: 'expiredate', width: 160, title: '到期时间', sort: true,
                    templet: function (d) {
                        if (d.expiredate == 1) {
                            return '永久';
                        } else {
                            return d.expiredate;
                        }
                    }
                },
                {
                    field: 'status', width: 100, title: '状态', sort: true,
                    templet: function (d) {
                        if (d.status == 1) {
                            return '<span class="layui-bg-blue layui-btn layui-btn-sm">开通</span>';
                        } else {
                            return '<span class="layui-bg-red layui-btn layui-btn-sm">禁用</span>';
                        }
                    }
                },
                {
                    field: 'api', width: 100, title: 'Api权限', sort: true,
                    templet: function (d) {
                        if (d.api == 1) {
                            return '<span class="layui-bg-blue layui-btn layui-btn-sm">开通</span>';
                        } else {
                            return '<span class="layui-bg-red layui-btn layui-btn-sm">禁用</span>';
                        }
                    }
                },
                {field: 'authkm', width: 180, title: '授权卡密'},
                {field: 'remark', width: 220, title: '备注', sort: true},
                ,{title:'操作', toolbar: '#currentTableBar', width:150}
            ]],done:function () {
                //绑定toolbar事件
                bindTableToolbarFunction();
            }

        });

        //表格重载
        function tableReload(params){
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: params
                ,done:function () {
                    bindTableToolbarFunction();
                }
            }, 'data');
        }

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            var params = {};
            params.authorizer = data.field.authorizer;
            params.auth = data.field.auth;
            params.status = data.field.status;
            params.authkm = data.field.authkm;
            //执行搜索重载
            tableReload(params);
            return false;
        });

        // ==================== 定义事件处理 ====================
        // 绑定事件集合处理(表格加载完毕和表格刷新的时候调用)
        function bindTableToolbarFunction() {
            // 监听编辑操作
            $(".data-edit-btn").on("click", function () {

                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;
                dataLength = data.length;
                if (dataLength < 1) {
                    layer.msg("请选择你要编辑的信息！", {icon: 5, time: 2000});
                    return false;
                } else if (dataLength > 1) {
                    layer.msg("只能选择一条记录！", {icon: 5, time: 2000});
                    return false;
                } else {
                    var index = layer.open({
                        title: '编辑授权信息',
                        type: 2,
                        shade: 0.2,
                        maxmin: true,
                        shadeClose: true,
                        area: ['100%', '100%'],
                        content: './auth-edit.php?id=' + data[0].id,
                        end: function(){ //此处用于演示
                            //执行重载
                            table.reload('currentTableId', {
                                page: {
                                    curr: 1
                                }
                                ,done:function () {
                                    bindTableToolbarFunction();
                                }
                            }, 'data');
                        }
                    });
                    //console.log(index);
                    $(window).on("resize", function () {
                        layer.full(index);
                    });
                    return false;
                }
            });

            // 监听删除操作
            $(".data-delete-btn").on("click", function () {
                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;
                dataLength = data.length;

                if (dataLength < 1) {
                    layer.msg("请选择你要删除的信息！", {icon: 5, time: 2000});
                    return false;
                }

                var params = new Array();
                $.each(data, function (i, val) {
                    params[i] = val.id;
                })
                layer.confirm('确定删除所选信息？', {
                    title: "提示",
                    icon: 3,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    getInfo(JSON.stringify(params));
                });

                return false;
            });

            // 监听清空全部操作
            $(".data-deleteAll-btn").on("click", function () {
                layer.confirm('确定清空所有授权信息？', {
                    title: "警告",
                    icon: 7,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    deleteAll();
                });
                return false;
            });
        }

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            //console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'showRobotApiText') {
                //console.log(data);
                showRobotApiText(data);
            }
            return false;
        });

        /**
         * 获取id对应的信息
         */
        function getInfo($ids) {
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            var param = {};
            param.type = 'del';
            param.id = $ids;
            $.ajax({
                url: '../../../public/action/admin/AuthAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: param,
                success: function (res) {

                    if (res.success == true) {
                        layer.msg('成功删除' + res.data.sucCount + "条、失败" + res.data.failCount + "条", {
                            icon: 6,
                            time: 2000
                        });
                        setTimeout(function () {
                            window.location.reload()
                        }, 3000);
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 1000});
                    }
                    layer.close(index);

                },
                error: function (data) {
                    layer.close(index);
                    layer.msg('服务器繁忙,请刷新重试！',{icon: 5,time:2000});
                }
            });//ajax结束
        }

        //清空全部
        function deleteAll() {
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            var param = {};
            param.type = 'alldel';
            $.ajax({
                url: '../../../public/action/admin/AuthAction.php',
                type: "get",
                dataType: "json",
                data: param,
                success: function (res) {

                    if (res.success == true) {
                        layer.msg(res.msg, {icon: 6, time: 1000});

                    } else {
                        layer.msg(res.msg, {icon: 5, time: 1000});
                    }
                    layer.close(index);
                    setTimeout(function () {
                        window.location.reload()
                    }, 2000);
                },
                error: function (data) {
                    layer.close(index);
                    layer.msg('服务器繁忙,请刷新重试！',{icon: 5,time:2000});
                }
            });//ajax结束
        }

        //展示机器人接口文本
        function showRobotApiText(rowData) {
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            var param = {};
            param.type = 'showapi';
            param.key = rowData.key;
            $.ajax({
                url: '../../../public/action/admin/AuthAction.php',
                type: "get",
                dataType: "json",
                data: param,
                success: function (res) {
                    if (res.success == true) {
                        var kmStr = "<br>";
                        $.each(res.data, function (i, val) {
                            kmStr += val + "<br><br>";
                        });
                        layer.open({
                            type: 1,
                            title: rowData.auth+'的接口文本 [全选复制]',
                            skin: 'layui-layer-rim', //加上边框
                            area: ['90%', '90%'], //宽高
                            content: '<pre class="layui-code">'+kmStr+"<br>"+'</pre>'
                        });
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 1000});
                    }
                    layer.close(index);
                },
                error: function (data) {
                    layer.close(index);
                    layer.msg('服务器繁忙,请刷新重试！',{icon: 5,time:2000});
                }
            });//ajax结束
        }
    });
</script>
<script>

</script>

</body>
</html>