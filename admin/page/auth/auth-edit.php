<?php
include("../../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form  layui-form-pane">
    <div class="layui-form-item">
        <label class="layui-form-label required">授权帐号</label>
        <div class="layui-input-block">
            <input type="hidden" id="id" name="id" lay-verify="required" lay-reqtext="id不能为空" placeholder="id" value=""
                   class="layui-input">
            <input type="text" id="auth" name="auth" lay-verify="required" lay-reqtext="授权帐号不能为空" placeholder="请输入授权帐号"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">联系人</label>
        <div class="layui-input-block">
            <input type="text" name="authorizer" lay-verify="required" lay-reqtext="联系人不能为空" placeholder="请输入联系人帐号"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">到期时间</label>
        <div class="layui-input-block">
            <input type="text"  id="expiredate" name="expiredate" lay-verify="" lay-reqtext="" placeholder="留空为永久授权"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">授权状态</label>
        <div class="layui-input-block">
            <select id="status" name="status">
                <option value="">请选择授权状态</option>
                <option value="0">禁用</option>
                <option value="1">开通</option>
            </select></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">Api权限</label>
        <div class="layui-input-inline">
            <select id="api" name="api">
                <option value="">请选择接口权限</option>
                <option value="0">禁用</option>
                <option value="1">开通</option>
            </select>

        </div>
        <div class="layui-form-mid layui-word-aux">允许机器人调用RobotApi,使用添加授权、删除授权、生成卡密等功能！</div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">备注信息</label>
        <div class="layui-input-block">
            <textarea id="remark" name="remark" class="layui-textarea" placeholder="请输入备注信息"></textarea>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="saveBtn">确定修改</button>
        </div>
    </div>
</div>
</div>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'layer', 'jquery','laydate'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.jquery;
        laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#expiredate' //指定元素
            ,type: 'datetime'
            ,format:'yyyy-MM-dd HH:mm:ss'
        });
        getInfo();

        /**
         * 获取id对应的信息
         */
        function getInfo() {
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            var param = {};
            param.type = 'all';
            param.id =<?php echo $_GET['id'];?>;
            param.page = 1;
            param.rows = 10;
            $("#id").val(param.id);
            $.ajax({
                url: '../../../public/action/admin/AuthAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: param,
                success: function (res) {
                    if (res.data.length > 0) {
                        $("input[name='auth']").val(res.data[0].auth);
                        $("input[name='authorizer']").val(res.data[0].authorizer);
                        $("input[name='expiredate']").val(res.data[0].expiredate==1?"":res.data[0].expiredate);
                        $("#status").find("option[value='" + res.data[0].status + "']").attr("selected", true);
                        $("#api").find("option[value='" + res.data[0].api + "']").attr("selected", true);
                        $("#remark").html(res.data[0].remark);
                        form.render();
                    } else {
                        layer.msg('数据加载失败,请重试!', {icon: 5, time: 1000});
                    }
                    layer.close(index);
                },
                error: function (data) {
                    layer.close(index);
                    layer.msg('服务器繁忙,请刷新重试！',{icon: 5,time:2000});
                }
            });//ajax结束
        }

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            var params = {};
            params = data.field;
            params.type = 'up';
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
           // console.log(params);
           // return false;
            $.ajax({
                url: '../../../public/action/admin/AuthAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: params,
                success: function (res) {
                    layer.close(index);
                    if (res.success) {
                        layer.alert(res.msg, {
                            skin: 'layui-layer-molv' //样式类名
                            , closeBtn: 0
                            , anim: 4 //动画类型
                        }, function () {
                            var iframeIndex = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(iframeIndex);
                        });
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 1000});
                    }
                },
                error: function (data) {
                    layer.close(index);
                    layer.msg('服务器繁忙,请刷新重试！',{icon: 5,time:2000});
                }
            });//ajax结束
            return false;
        });
    });
</script>
</body>
</html>