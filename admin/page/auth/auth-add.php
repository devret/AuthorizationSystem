<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-14 20:26
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */
include("../../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form  layui-form-pane">
    <div class="layui-form-item">
        <label class="layui-form-label required">授权帐号</label>
        <div class="layui-input-block">
            <input type="text" id="auth" name="auth" lay-verify="required" lay-reqtext="授权帐号不能为空" placeholder="请输入授权帐号"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">联系人</label>
        <div class="layui-input-block">
            <input type="text" name="authorizer" lay-verify="required" lay-reqtext="联系人不能为空" placeholder="请输入联系人帐号"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">到期时间</label>
        <div class="layui-input-block">
            <input type="text"  id="expiredate" name="expiredate" lay-verify="" lay-reqtext="" placeholder="留空为永久授权"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">授权状态</label>
        <div class="layui-input-block">
            <select id="status" name="status">
                <option value="1" selected>开通</option>
                <option value="0">禁用</option>

            </select></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">Api权限</label>
        <div class="layui-input-block">
            <select id="api" name="api">
                <option value="0">禁用</option>
                <option value="1">开通</option>
            </select>
        </div>

    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">备注信息</label>
        <div class="layui-input-block">
            <textarea id="remark" name="remark" class="layui-textarea" placeholder="请输入备注信息&#13;&#10;Api权限：允许机器人调用RobotApi,使用添加授权、删除授权、生成卡密等功能！"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="saveBtn">确定添加</button>
        </div>
    </div>
</div>
</div>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'layer', 'jquery','laydate'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.jquery,
         laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#expiredate' //指定元素
            ,type: 'datetime'
            ,format:'yyyy-MM-dd HH:mm:ss'
        });
        //监听提交
        form.on('submit(saveBtn)', function (data) {
            var params = {};
            params = data.field;
            params.type = 'addnotkm';
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            $.ajax({
                url: '../../../public/action/admin/AuthAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: params,
                success: function (res) {
                    layer.close(index);
                    if (res.success) {
                        layer.msg(res.msg,{icon: 6, time: 2000});
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 1000});
                    }
                },
                error: function (data) {
                    layer.msg('系统错误!', {icon: 5, time: 1000});
                }
            });//ajax结束
            return false;
        });
    });
</script>
</body>
</html>