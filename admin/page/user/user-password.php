<?php
include("../../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改密码</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
    <style>
        .layui-form-item .layui-input-company {width: auto;padding-right: 10px;line-height: 38px;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <div class="layui-form  layui-form-pane">
            <div class="layui-form-item" >
                <label class="layui-form-label required">新帐号</label>
                <div class="layui-input-block">
                    <input type="username" name="username" lay-verify="required"  placeholder="请输入新管理员帐号"  value="" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label required">新密码</label>
                <div class="layui-input-block">
                    <input type="password" name="password" lay-verify="required" lay-reqtext="新的密码不能为空" placeholder="请输入新管理员密码"  value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="saveBtn">确认修改</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script src="../../js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['form','jquery'], function () {
        var form = layui.form,
            layer = layui.layer,
            $=layui.jquery;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            var params ={};
            params = data.field;
            params.type="up";

            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            $.ajax({
                url: '../../../public/action/admin/UserAction.php',
                type:"get",
                dataType:"json",
                // contentType: "application/json; charset=utf-8",
                data:params,
                success: function (res) {
                    layer.close(index);
                    if(res.success!=true){
                        layer.msg(res.msg, {icon: 5});
                    }else {
                        layer.msg(res.msg, {icon: 6});
                    }
                },
                error:function (data) {
                    layer.close(index);
                    layer.msg('服务器繁忙,请刷新重试！',{icon: 5,time:2000});
                }
            });//ajax结束
            return false;
        });

    });
</script>
</body>
</html>