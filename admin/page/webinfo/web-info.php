<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-15 15:13
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */
include("../../../public/config/logincheck.php");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form  layui-form-pane">
    <div class="layui-form-item">
        <label class="layui-form-label required">顶部标题</label>
        <div class="layui-input-block">
            <input type="text" id="headtitle" name="headtitle" lay-verify="required" lay-reqtext="顶部标题不能为空" placeholder="请输入顶部标题"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">底部标题</label>
        <div class="layui-input-block">
            <input type="text" name="foottitle" lay-verify="required" lay-reqtext="网站底部标题不能为空" placeholder="请输入网站底部标题"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">首页音乐</label>
        <div class="layui-input-block">
            <input type="text" name="music" lay-verify="required" lay-reqtext="" placeholder="请输入音乐链接"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">联系人QQ</label>
        <div class="layui-input-block">
            <input type="text" name="contact" lay-verify="required" lay-reqtext="" placeholder="请输入联系人帐号"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">选择模板</label>
        <div class="layui-input-block">
            <select name="template" lay-verify="">

            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="saveBtn">确定修改</button>
        </div>
    </div>
</div>
</div>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'layer', 'jquery'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.jquery;

        getWebInfo();

        /**
         * 获取id对应的信息
         */
        function getWebInfo() {
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            var param = {};
            param.type = 'get';
            $("#id").val(param.id);
            $.ajax({
                url: '../../../public/action/admin/ConfigAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: param,
                success: function (res) {
                    if (res.success) {
                        $("input[name='headtitle']").val(res.data.headtitle);
                        $("input[name='foottitle']").val(res.data.foottitle);
                        $("input[name='music']").val(res.data.music);
                        $("input[name='contact']").val(res.data.contact);

                        var opt = '<option value="'+res.data.template+'">'+res.data.template+'</option>';
                        $.each(res.data.dir,function (i,val) {
                            if(res.data.template!=val){
                                opt += '<option value="'+val+'">'+val+'</option>';
                            }
                        });
                        $("select[name='template']").empty().html(opt);
                        form.render('select');
                        //console.log(res);
                    } else {
                        layer.msg('数据加载失败,请重试!', {icon: 5, time: 1000});
                    }
                    layer.close(index);
                },
                error: function (data) {
                    layer.msg('系统错误!', {icon: 5, time: 1000});
                }
            });//ajax结束
        }

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            var params = {};
            params = data.field;
            params.type = 'up';
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            $.ajax({
                url: '../../../public/action/admin/ConfigAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: params,
                success: function (res) {
                    layer.close(index);
                    if (res.success) {
                        layer.msg(res.msg, {icon: 6, time: 1000});
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 1000});
                    }
                },
                error: function (data) {
                    layer.msg('系统错误!', {icon: 5, time: 1000});
                }
            });//ajax结束
            return false;
        });
    });
</script>
</body>
</html>