<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-15 15:46
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */
include("../../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>系统信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<blockquote class="layui-elem-quote" style="margin-top: 10px;">
    晨风机器人软件、配置、插件、接口授权系统
</blockquote>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend>系统信息</legend>
</fieldset>

<div class="layui-collapse" lay-filter="filter">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">当前版本:<span><?php echo SYSTEM_VERSION; ?></span></h2>
        <div class="layui-colla-content">
            <ul class="layui-timeline">
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年10月03日</h3>
                        <p> 优化RobotApi接口,支持快速更改为其它框架机器人可使用的接口</p>
                        <p> 增加json接口添加永久、期限卡密(便于支付生成卡密对接)</p>
                        <p> 增加json接口查询卡密信息</p>
                        <p>增加json接口查询授权信息</p>
                        <p>增加RobotApi接口查询授权显示到期时间</p>
                        <p>修复RobotApi生成期限卡密的错误提示</p>
                        <p>修复某些主机使用安装程序提示报错的bug</p>
                        <p>修复某些主机数据库版本低无法导入数据信息的bug</p>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年06月18日</h3>
                        <p> 增加前台一套少女系UI，后台-网站管理-界面管理-选择v3模板</p>
                        <p> 增加配置授权接口到期时间验证</p>
                        <p> 修复授权列表页面当进行筛选后，表头的编辑、删除和X按钮失效的bug</p>
                        <p>优化前台页面显示，访问具体界面按照以下方式访问<br>
                        <blockquote class="layui-elem-quote">
                            访问主页：域名<br>
                            访问授权页：域名/?m=sq<br>
                            访问授权查询页：域名/?m=sqcx<br>
                        </blockquote>
                        </p>
                        <p>优化后台主框架，升级框架到layuiminiV2版</p>
                        <p>优化错误提示</p>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年03月22日</h3>
                        <p>增加RobotApi授权冻结、授权激活</p>
                        <p> 增加RobotApi生成期限卡密</p>
                        <p> 增加RobotApi生成永久卡密</p>
                        <p>增加RobotApi获取机器人key功能</p>
                        <p> 增加RobotApi清空指定主人下的所有授权帐号</p>
                        <p> 增加机器人调用RobotApi授权时间到期验证</p>
                        <p> 增加在授权列表的最右边可根据授权帐号自动导出接口文本</p>
                        <p> 增加机器人配置授权验证接口</p>
                        <p> 增加接口调用次数统计,具体使用请查询api.html</p>
                        <p> 增加域名授权验证接口-可用于代刷授权验证，具体使用请查询api.html</p>
                        <p> 增加管理员手动添加授权时,是否开通Api权限功能</p>
                        <p> 修复RobotApi我的授权权能够修改接口参数指定授权联系人的bug</p>
                        <p> 修复RobotApi删除我的授权能够修改接口参数指定授权联系人的bug</p>
                        <p> 修复RobotApi更换我的授权能够修改接口参数指定授权联系人的bug</p>
                        <p> 修复卡密授权期限异常问题,且手动添加卡密时候天数只能输入数字</p>
                        <p> 优化在线更新功能</p>
                        <p> 优化安装程序,提高执行效率</p>
                        <p> 优化后台授权列表页,增加清空授权,将原本在最右边的编辑和删除移动到表头部</p>
                        <p> 优化后台卡密删除,增加删除确认对话框提示</p>
                        <p> 优化RobotApi错误提示</p>
                        <p> 优化卡密列表界面显示,颜色区分未被使用和已经使用的卡密</p>
                        <p> 优化数据库操作</p>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年03月12日</h3>
                        <p> 优化数据库操作层</p>
                        <p> 增加在线更新功能</p>
                        <p> 修复禁用授权后还能机器人还能调用Api接口</p>
                        <p>优化机器人调用"我的授权"指令响应排版显示异常问题</p>
                        <p>修复模板v2授权和授权查询页面404</p>
                        <p>修复更改授权信息会把授权时间弄错的bug</p>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年2月23日</h3>
                        <p>增加RoboApi功能接口</p>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年2月17日</h3>
                        <p>
                            <a href="https://sixcloud.co/">6云授权系统</a>新版本1.0发布
                            <br>采用layui、jquer框架开发
                            <br>畅享丝滑 <i class="layui-icon"></i>
                            <br>采用layui、jquer框架开发
                        </p>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年2月17日</h3>
                        <p>修复登录bug</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend>在线更新</legend>
</fieldset>
<button id="lineUpdate" type="button" class="layui-btn   layui-btn-fluid layui-btn-normal">版本检测</button>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend>手动更新</legend>
</fieldset>
<a href="https://gitee.com/devret/AuthorizationSystem/blob/master/UPDATE.md" target="_blank"
   class="layui-btn layui-btn-fluid">查询</a>
<blockquote class="layui-elem-quote" style="margin-top: 10px;">
    -<a href="https://sixcloud.co/"> 6云科技</a>- https://sixcloud.co/
</blockquote>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['element', 'layer', 'jquery'], function () {
        var form = layui.form,
            layer = layui.layer,
            element = layui.element,
            $ = layui.jquery;

        $("#lineUpdate").click(function () {
            var param = {};
            param.type = 'check';
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            $.ajax({
                url: '../../../public/action/admin/VersionAction.php',
                type: "get",
                dataType: "json",
                data: param,
                success: function (res) {
                    layer.close(index);
                    if (res.success) {
                        var msg = '更新时间:' + res.data.time + '<br>' + '更新内容:<br>' + res.data.msg;
                        layer.confirm(msg, {
                            title: '最新版本：' + res.data.version
                            , btn: ['确认更新', '取消'] //可以无限个按钮
                        }, function (index, layero) {

                            newUpdate();
                        }, function (index) {

                        });
                    } else {
                        layer.msg('数据加载失败,请重试!', {icon: 5, time: 1000});
                    }
                },
                error: function (data) {
                    layer.close(index);
                    layer.msg('服务器繁忙,请刷新重试！',{icon: 5,time:2000});
                }
            });//ajax结束
        });//按钮点击

        function newUpdate() {
            var param = {};
            param.type = 'update';
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            $.ajax({
                url: '../../../public/action/admin/VersionAction.php',
                type: "get",
                dataType: "json",
                data: param,
                success: function (res) {
                    if (res.success) {
                        layer.msg(res.msg, {icon: 6, time: 2000});
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 3000});
                    }
                    layer.close(index);
                },
                error: function (data) {
                    layer.close(index);
                    layer.msg('服务器不支持在线解压!<br>请手动解压网站根路径下的update.zip文件', {icon: 5, time: 4000});
                }
            });//ajax结束
        }
    });
</script>
</body>
</html>