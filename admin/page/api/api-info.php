<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-15 16:55
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

include("../../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>Robot接口</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<blockquote class="layui-elem-quote" style="margin-top: 10px;">
    晨风机器人软件、配置、插件、接口授权系统
</blockquote>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend>接口信息</legend>
</fieldset>

<div class="layui-collapse" lay-filter="filter">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">系统接口:<span><?php echo API_VERSION; ?></span></h2>
        <div class="layui-colla-content">
            <blockquote class="layui-elem-quote" style="margin-top: 10px;">
                提示：以下接口文本可在<span style="color: red">授权列表</span>的<span style="color: red">最右边</span>根据授权帐号自动生成！
            </blockquote>
            <ul class="layui-timeline">
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年03月22日</h3>
                        <ul>
                            <li>
                                授权冻结* 【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqdj&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$】%机器人主人要求是(权限要求:机器人主人)%
                            </li>
                            <li>
                                授权激活* 【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqjh&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$】%机器人主人要求是(权限要求:机器人主人)%
                            </li>
                            <li>
                                生成期限卡密* 【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=kmaddb&key=<span style="color: red">机器人的key</span>&num=$回声段落1$&day=$回声段落2$】%创始人要求是(权限要求:创始人)%
                            </li>
                            <li>
                                生成永久卡密* 【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=kmadda&key=<span style="color: red">机器人的key</span>&num=$回声段落1$】%创始人要求是(权限要求:创始人)%
                            </li>
                            <li>
                                获取key* 【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=getkey&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$】
                            </li>
                            <li>
                                清空授权*
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqclear&key=<span style="color: red">机器人的key</span>&zrqq=$回声段落1$】%创始人要求是(权限要求:创始人)%
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年2月23日</h3>
                        <ul>
                            <li>
                                更换主人*
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqghzr&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$&newzrqq=$回声段落2$】%机器人主人要求是(权限要求:机器人主人)%
                            </li>
                            <li>
                                更换授权*
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqgh&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$&newqq=$回声段落2$】%机器人主人要求是(权限要求:机器人主人)%
                            </li>
                            <li>
                                更换我的授权*
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqghmy&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$&newqq=$回声段落2$】
                            </li>
                            <li>
                                删除授权*
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqsc&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$】%机器人主人要求是(权限要求:机器人主人)%
                            </li>
                            <li>
                                删除我的授权*
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqscmy&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$】
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年2月17日</h3>
                        <ul>
                            <li>
                                添加永久授权*
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqtj&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$&zrqq=$回声段落2$】%机器人主人要求是(权限要求:机器人主人)%
                            </li>
                            <li>
                                查授权*
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqcx&key=<span style="color: red">机器人的key</span>&qq=$回声段落1$】
                            </li>
                            <li>
                                我的授权
                                【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotApi.php?type=sqmy&key=<span style="color: red">机器人的key</span>】
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年2月16日</h3>
                        <ul>
                            <li>
                                <blockquote class="layui-elem-quote" style="margin-top: 10px;color:red">
                                    注意：使用以上接口,机器人必须已经添加授权,且开通Api权限功能，方可使用！
                                </blockquote>
                            </li>
                            <li>
                                <blockquote class="layui-elem-quote" style="margin-top: 10px;">
                                    注意：如果手动复制以上接口请替换掉"<span style="color: red">机器人的key</span>"几个字,具体机器的key请查询授权信息！
                                </blockquote>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="layui-collapse" lay-filter="filter">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">配置接口:<span>1.0</span></h2>
        <div class="layui-colla-content">
            <ul class="layui-timeline">
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年03月22日</h3>
                        <ul>
                            <li>
                                配置授权检测 【显示网址内容<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/public/action/api/RobotConfigApi.php】
                                <div class="layui-field-box">
                                    使用机器人调用配置检测接口会返回"<span style="color: red">已授权</span>"、"<span
                                            style="color: red">未授权</span>"、"<span
                                            style="color: red">授权被禁用</span>"3种结果，
                                    具体需求可自行修改RobotConfigApi.php文件
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>


<div class="layui-collapse" lay-filter="filter">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">插件版本:<span>1.0</span></h2>
        <div class="layui-colla-content">
            <ul class="layui-timeline">
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年2月17日</h3>
                        <ul>
                            <li>
                                示例文本
                            </li>
                            <li>
                                示例文本
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">2020年2月17日</h3>
                        <ul>
                            <li>
                                示例文本
                            </li>
                            <li>
                                示例文本
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<blockquote class="layui-elem-quote" style="margin-top: 10px;">
    -<a href="https://sixcloud.co/"> 6云科技</a>- https://sixcloud.co/
</blockquote>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['element', 'layer', 'jquery'], function () {
        var form = layui.form,
            layer = layui.layer,
            element = layui.element,
            $ = layui.jquery;

    });
</script>
</body>
</html>