<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-15 2:10
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

include("../../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>卡密添加</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form  layui-form-pane">
    <div class="layui-form-item">
        <label class="layui-form-label required">生成数量</label>
        <div class="layui-input-block">
            <input type="hidden" id="createuserid" name="createuserid" lay-verify="required" value="<?php session_start();echo $_SESSION['id']; ?>" class="layui-input">
            <input type="text" id="createnum" name="createnum" lay-verify="number" lay-reqtext="卡密数量不能为空"
                   placeholder="请输入卡密数量"
                   value="" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">时长(天)</label>
        <div class="layui-input-block">
            <input type="text" id="expiredate" name="expiredate" lay-verify="number" lay-reqtext=""
                   placeholder="0为永久授权"
                   value="" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="saveBtn">确定添加</button>
        </div>
    </div>
</div>
</div>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'layer', 'jquery'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.jquery;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            var params = {};
            params = data.field;
            params.type = 'add';
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            $.ajax({
                url: '../../../public/action/admin/KmAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: params,
                success: function (res) {
                    layer.close(index);
                    if (res.success) {
                        //console.log(res.data[0][1][1]);
                        var kmStr = "";
                        $.each(res.data, function (i, val) {
                            kmStr += val[1][1] + "<br>";
                        });
                        layer.open({
                            type: 1,
                            title: '卡密列表',
                            skin: 'layui-layer-rim', //加上边框
                            area: ['80%', '90%'], //宽高
                            content: kmStr
                        });
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 1000});
                    }
                },
                error: function (data) {
                    layer.close(index);
                    layer.msg('系统错误!', {icon: 5, time: 1000});
                }
            });//ajax结束
            return false;
        });
    });
</script>
</body>
</html>