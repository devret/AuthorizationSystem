<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-02-14 23:19
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

include("../../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../../css/public.css" media="all">
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm  data-delete-btn">删除所选</button>
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-all-btn">清空全部</button>
            </div>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>
<script src="../../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'table', 'layer'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            layer = layui.layer;

        table.render({
            elem: '#currentTableId',
            url: '../../../public/action/admin/KmAction.php',
            where: {type: 'all'},
            request: {
                pageName: 'page' //页码的参数名称，默认：page
                , limitName: 'rows' //每页数据量的参数名，默认：limit
            },
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports'],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 10,
            page: true,
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID', sort: true, hide: true},
                {field: 'card', width: 215, title: '卡密'},
                {
                    field: 'use', width: 176, title: '状态', sort: true,
                    templet: function (d) {
                        //console.log(d);
                        if (d.use == 0) {
                            return '<span class="layui-bg-blue layui-btn  layui-btn-fluid"> 未使用</span>';
                        } else {
                            if (d.auth != null) {
                                return '<span class="layui-bg-red layui-btn layui-btn-fluid"> 已授权:' + d.auth + '</span>';
                            } else {
                                return '<span class="layui-bg-red layui-btn layui-btn-fluid"> 已使用</span>';
                            }
                        }
                    }
                },
                {field: 'createusername', width: 160, title: '生成人', sort: true},
                {
                    field: 'expiredate', width: 131, title: '时长(天)', sort: true,
                    templet: function (d) {
                        if (d.expiredate == 0) {
                            return '永久';
                        } else {
                            return d.expiredate;
                        }
                    }
                },
                {field: 'usedate', width: 270, title: '使用时间', sort: true},

                {field: 'createdate', title: '添加时间', width: 270, sort: true}

            ]]
        });

        // 监听删除全部
        $(".data-delete-all-btn").on("click", function () {
            layer.confirm('确定要清空全部卡密吗？', {
                title: "警告",
                icon: 7,
                btn: ['确定', '取消'] //按钮
            }, function () {
                deleteAll();
            }, function () {

            });

            function deleteAll() {
                var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
                var param = {};
                param.type = 'alldel';
                $.ajax({
                    url: '../../../public/action/admin/KmAction.php',
                    type: "get",
                    dataType: "json",
                    data: param,
                    success: function (res) {

                        if (res.success == true) {
                            layer.msg(res.msg, {icon: 6, time: 1000});

                        } else {
                            layer.msg(res.msg, {icon: 5, time: 1000});
                        }
                        layer.close(index);
                        setTimeout(function () {
                            window.location.reload()
                        }, 2000);
                    },
                    error: function (data) {
                        layer.msg('系统错误!', {icon: 5, time: 1000});
                    }
                });//ajax结束
            }
        });

        // 监听删除所选择
        $(".data-delete-btn").on("click", function () {
            var checkStatus = table.checkStatus('currentTableId')
                , data = checkStatus.data;
            dataLength = data.length;

            if (dataLength < 1) {
                layer.msg("请选择你要删除的信息！", {icon: 5, time: 2000});
                return false;
            }
            var params = new Array();
            $.each(data, function (i, val) {
                params[i] = val.id;

            })

            layer.confirm('确定删除所选信息？', {
                title: "提示",
                icon: 3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                getInfo(JSON.stringify(params));
            });
            return false;
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            // console.log(obj)
        });

        /**
         * 获取id对应的信息
         */
        function getInfo($ids) {
            var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
            var param = {};
            param.type = 'del';
            param.id = $ids;
            $.ajax({
                url: '../../../public/action/admin/KmAction.php',
                type: "get",
                dataType: "json",
                // contentType: "application/json; charset=utf-8",
                data: param,
                success: function (res) {
                    if (res.success == true) {
                        layer.msg('成功删除' + res.data.sucCount + "条、失败" + res.data.failCount + "条", {
                            icon: 6,
                            time: 2000
                        });
                        setTimeout(function () {
                            window.location.reload()
                        }, 3000);
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 1000});
                    }
                    layer.close(index);

                },
                error: function (data) {
                    layer.msg('系统错误!', {icon: 5, time: 1000});
                }
            });//ajax结束
        }
    });
</script>
<script>

</script>

</body>
</html>