<?php
include("../../public/config/logincheck.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>首页</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="../lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="../css/public.css" media="all">
    <style>
        .layui-card {
            border: 1px solid #f2f2f2;
            border-radius: 5px;
        }

        .icon {
            margin-right: 10px;
            color: #1aa094;
        }

        .icon-cray {
            color: #ffb800 !important;
        }

        .icon-blue {
            color: #1e9fff !important;
        }

        .icon-tip {
            color: #ff5722 !important;
        }

        .layuimini-qiuck-module {
            text-align: center;
            margin-top: 10px
        }

        .layuimini-qiuck-module a i {
            display: inline-block;
            width: 100%;
            height: 60px;
            line-height: 60px;
            text-align: center;
            border-radius: 2px;
            font-size: 30px;
            background-color: #F8F8F8;
            color: #333;
            transition: all .3s;
            -webkit-transition: all .3s;
        }

        .layuimini-qiuck-module a cite {
            position: relative;
            top: 2px;
            display: block;
            color: #666;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            font-size: 14px;
        }

        .welcome-module {
            width: 100%;
            height: 210px;
        }

        .panel {
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 3px;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05)
        }

        .panel-body {
            padding: 10px
        }

        .panel-title {
            margin-top: 0;
            margin-bottom: 0;
            font-size: 12px;
            color: inherit
        }

        .label {
            display: inline;
            padding: .2em .6em .3em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
            margin-top: .3em;
        }

        .layui-red {
            color: red
        }

        .main_btn > p {
            height: 40px;
        }

        .layui-bg-number {
            background-color: #F8F8F8;
        }

        .layuimini-notice:hover {
            background: #f6f6f6;
        }

        .layuimini-notice {
            padding: 7px 16px;
            clear: both;
            font-size: 12px !important;
            cursor: pointer;
            position: relative;
            transition: background 0.2s ease-in-out;
        }

        .layuimini-notice-title, .layuimini-notice-label {
            padding-right: 70px !important;
            text-overflow: ellipsis !important;
            overflow: hidden !important;
            white-space: nowrap !important;
        }

        .layuimini-notice-title {
            line-height: 28px;
            font-size: 14px;
        }

        .layuimini-notice-extra {
            position: absolute;
            top: 50%;
            margin-top: -8px;
            right: 16px;
            display: inline-block;
            height: 16px;
            color: #999;
        }
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md8">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md6">
                        <div class="layui-card">
                            <div class="layui-card-header"><i class="fa fa-warning icon"></i>数据统计
                                <span id="loading" class="layui-badge">加载中···</span>

                                </div>
                            <div class="layui-card-body">
                                <div class="welcome-module">
                                    <div class="layui-row layui-col-space10">
                                        <div class="layui-col-xs6">
                                            <div class="panel layui-bg-number">
                                                <div class="panel-body">
                                                    <div class="panel-title">
                                                        <span class="label pull-right layui-bg-blue">实时</span>
                                                        <h5>授权统计</h5>
                                                    </div>
                                                    <div class="panel-content">
                                                        <h1 id="authCount" class="no-margins">···</h1>
                                                        <small>当前分类总记录数</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-col-xs6">
                                            <div class="panel layui-bg-number">
                                                <div class="panel-body">
                                                    <div class="panel-title">
                                                        <span class="label pull-right layui-bg-cyan">实时</span>
                                                        <h5>卡密统计</h5>
                                                    </div>
                                                    <div class="panel-content">
                                                        <h1 id="kmCount" class="no-margins">···</h1>
                                                        <small>当前分类总记录数</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-col-xs6">
                                            <div class="panel layui-bg-number">
                                                <div class="panel-body">
                                                    <div class="panel-title">
                                                        <span class="label pull-right layui-bg-orange">实时</span>
                                                        <h5>授权状态-正常</h5>
                                                    </div>
                                                    <div class="panel-content">
                                                        <h1 id="normalCount" class="no-margins">···</h1>
                                                        <small>当前分类总记录数</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-col-xs6">
                                            <div class="panel layui-bg-number">
                                                <div class="panel-body">
                                                    <div class="panel-title">
                                                        <span class="label pull-right layui-bg-green">实时</span>
                                                        <h5>授权状态-禁用</h5>
                                                    </div>
                                                    <div class="panel-content">
                                                        <h1 id="disableCount" class="no-margins">···</h1>
                                                        <small>当前分类总记录数</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md6">
                        <div class="layui-card">
                            <div class="layui-card-header"><i class="fa fa-credit-card icon icon-blue"></i>快捷入口</div>
                            <div class="layui-card-body">
                                <div class="welcome-module">
                                    <div class="layui-row layui-col-space10 layuimini-qiuck">
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" layuimini-content-href="page/auth/auth-add.php"
                                               data-title="添加授权" data-icon="fa fa-window-maximize">
                                                <i class="fa fa-window-maximize"></i>
                                                <cite>添加授权</cite>
                                            </a>
                                        </div>
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" layuimini-content-href="page/km/km-add.php"
                                               data-title="添加卡密" data-icon="fa fa-gears">
                                                <i class="fa fa-gears"></i>
                                                <cite>添加卡密</cite>
                                            </a>
                                        </div>
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" layuimini-content-href="page/auth/auth-list.php"
                                               data-title="授权列表" data-icon="fa fa-file-text">
                                                <i class="fa fa-file-text"></i>
                                                <cite>授权列表</cite>
                                            </a>
                                        </div>
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" layuimini-content-href="page/km/km-list.php"
                                               data-title="卡密列表" data-icon="fa fa-dot-circle-o">
                                                <i class="fa fa-dot-circle-o"></i>
                                                <cite>卡密列表</cite>
                                            </a>
                                        </div>
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;"
                                               layuimini-content-href="https://gitee.com/devret/AuthorizationSystem"
                                               data-title="系统更新" data-icon="fa fa-calendar">
                                                <i class="fa fa-calendar"></i>
                                                <cite>系统更新</cite>
                                            </a>
                                        </div>
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" onclick="chat();" data-title="反馈建议"
                                               data-icon="fa fa-hourglass-end">
                                                <i class="fa fa-hourglass-end"></i>
                                                <cite>反馈建议</cite>
                                            </a>
                                        </div>
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" layuimini-content-href="page/webinfo/web-info.php" data-title="界面管理"
                                               data-icon="fa fa-snowflake-o">
                                                <i class="fa fa-snowflake-o"></i>
                                                <cite>界面管理</cite>
                                            </a>
                                        </div>
                                        <div class="layui-col-xs3 layuimini-qiuck-module">
                                            <a href="javascript:;" target="_blank" layuimini-content-href="http://sq.sixcloud.co/api.html" data-title="接口信息"
                                               data-icon="fa fa-shield">
                                                <i class="fa fa-shield"></i>
                                                <cite>对接文档</cite>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="layui-col-md12 ">
                        <div class="layui-card">
                            <div class="layui-card-header"><i class="fa fa-line-chart icon"></i>报表统计</div>
                            <div class="layui-card-body">
                                <div id="echarts-records" style="width: 100%;min-height:500px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="layui-col-md4">

                <div class="layui-card">
                    <div class="layui-card-header"><i class="fa fa-bullhorn icon icon-tip"></i>系统公告</div>
                    <div class="layui-card-body layui-text">
                        <div class="layuimini-notice">
                            <div class="layuimini-notice-title">欢迎使用Six云网络授权系统</div>
                            <div class="layuimini-notice-extra">2020-02-21</div>
                            <div class="layuimini-notice-content layui-hide">
                                有BUG?<a style="font-size: 20px;color: red" href="https://jq.qq.com/?_wv=1027&k=5J3SSgO">点击反馈建议</a><br>
                                 群：1031929745<br>
                                 界面足够简洁清爽，响应式且适配手机端。<br>
                                 页面支持多配色方案，可自行选择喜欢的配色。<br>
                                 支持多tab，可以打开多窗口。<br>
                                 刷新页面会保留当前的窗口，并且会定位当前窗口对应左侧菜单栏。<br>
                                 首页采用可视化数据统计，数据状态一目了然。<br>
                                 一键修改前台模板页信息，方便快捷。<br>
                                 后台管理支持切换前台用户模板，随时换“装”，主页不在单调。<br>
                                 可随时增加自己喜欢的模板，只需要一几句话即可调用网站信息配置前端模板。<br>
                            </div>
                        </div>
                        <div class="layuimini-notice">
                            <div class="layuimini-notice-title">免费机器人软件、配置、插件、接口</div>
                            <div class="layuimini-notice-extra">2020-02-21</div>
                            <div class="layuimini-notice-content layui-hide">
                                更多免费资源加入群"逆-学破解：<span style="font-size: 20px;color: red">66902847</span><br>
                                <a  style="font-size: 20px;color: red" href="https://jq.qq.com/?_wv=1027&k=5QxDRZK">点击加群</a><br>
                                专属于机器人软件、配置、插件、接口的交流平台<br>
                            </div>
                        </div>
                        <div class="layuimini-notice">
                            <div class="layuimini-notice-title">配置、域名对接文档</div>
                            <div class="layuimini-notice-extra">2020-06-18</div>
                            <div class="layuimini-notice-content layui-hide">
                                各类对接方法请查看以下路径的源文件，有详细描述<br>
                                配置授权接口:<br>/public/action/api/RobotConfigApi.php<br><br>
                                域名/代刷网授权接口：<br>/public/action/api/DomainCheckApi.php<br>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-card">
                    <div class="layui-card-header"><i class="fa fa-fire icon"></i>服务器信息</div>
                    <div class="layui-card-body layui-text">
                        <table class="layui-table">
                            <colgroup>
                                <col width="160">
                                <col>
                            </colgroup>
                            <tbody>
                            <tr>
                                <td>PHP 版本</td>
                                <td>
                                    <?php echo phpversion() ?>
                                </td>
                            </tr>
                            <tr>
                                <td>MySQL 版本</td>
                                <td><?php echo $_SESSION['webConfig']['mysqlVersion']; ?></td>
                            </tr>
                            <tr>
                                <td>服务器软件：</td>
                                <td><?php echo $_SERVER['SERVER_SOFTWARE']; ?></td>
                            </tr>

                            <tr>
                                <td>POST限制：</td>
                                <td>
                                    <?php echo ini_get('post_max_size'); ?></td>
                            </tr>
                            <tr>
                                <td>你的IP地址是：</td>
                                <td>
                                    <?php
                                    echo @$_SERVER['REMOTE_ADDR'];
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>服务器时间：</td>
                                <td style="padding-bottom: 0;">
                                    <?php  echo date("Y年m月d日 h:i:sa"); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>本地时间：</td>
                                <td id="times" style="padding-bottom: 0;">

                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="layui-card">
                    <div class="layui-card-header"><i class="fa fa-paper-plane-o icon"></i>面板信息</div>
                    <div class="layui-card-body layui-text layadmin-text">
                        <blockquote class="layui-elem-quote" style="margin-top: 10px;">
                            <p>系统版本：<a  href="https://sixcloud.co/">
                                    Six云科技 </a><?php echo SYSTEM_VERSION; ?></p>
                            <br>
                            <p> 最近更新时间：<?php echo UPDATE_TIEM;?></p>
                        </blockquote>
                        <blockquote class="layui-elem-quote" style="margin-top: 10px;">
                            喜欢此授权系统的可以给我的<a href="https://gitee.com/devret/AuthorizationSystem/">Gitee</a>和<a href="https://github.com/wyx176/AuthorizationSystem.git">GitHub</a>加个Star支持一下
                        </blockquote>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="../lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script src="../js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['layer','miniTab', 'echarts', 'jquery'], function () {
        var $ = layui.jquery,
            layer = layui.layer,
            miniTab = layui.miniTab,
            echarts = layui.echarts;
        miniTab.listen();

        /**
         * 查看公告信息
         **/
        $('body').on('click', '.layuimini-notice', function () {
            var title = $(this).children('.layuimini-notice-title').text(),
                noticeTime = $(this).children('.layuimini-notice-extra').text(),
                content = $(this).children('.layuimini-notice-content').html();
            var html = '<div style="padding:15px 20px; text-align:justify; line-height: 22px;border-bottom:1px solid #e2e2e2;background-color: #2f4056;color: #ffffff">\n' +
                '<div style="text-align: center;margin-bottom: 20px;font-weight: bold;border-bottom:1px solid #718fb5;padding-bottom: 5px"><h4 class="text-danger">' + title + '</h4></div>\n' +
                '<div style="font-size: 12px">' + content + '</div>\n' +
                '</div>\n';
            parent.layer.open({
                type: 1,
                title: '系统公告' + '<span style="float: right;right: 1px;font-size: 12px;color: #b1b3b9;margin-top: 1px">' + noticeTime + '</span>',
                area: '300px;',
                shade: 0.8,
                id: 'layuimini-notice',
                btn: ['查看', '取消'],
                btnAlign: 'c',
                moveType: 1,
                content: html,
                success: function (layero) {
                    var btn = layero.find('.layui-layer-btn');
                    btn.find('.layui-layer-btn0').attr({
                        href: 'https://jq.qq.com/?_wv=1027&k=5J3SSgO',
                        target: '_blank'
                    });
                }
            });
        });


        var statistics = {};
        var index = layer.load(0, {shade: [0.2, '#393D49']}, {shadeClose: true}); //0代表加载的风格，支持0-2
        $.ajax({
            url: '../../../public/action/admin/ConfigAction.php',
            type: "get",
            dataType: "json",
            async: false,
            data: {type: "getStatistics"},
            success: function (res) {
                layer.close(index);
                $("#loading").hide();
                if (res.success) {
                    $("#authCount").text(res.data.authCount);
                    $("#kmCount").text(res.data.kmCount);
                    $("#normalCount").text(res.data.normalCount);
                    $("#disableCount").text(res.data.disableCount);
                    statistics = res.data;
                } else {
                    layer.msg(res.msg, {icon: 5, time: 1000});
                }
            },
            error: function (data) {
                layer.close(index);
                layer.msg('系统错误!', {icon: 5, time: 1000});
            }
        });//ajax结束

        /**
         * 报表功能
         */
        var data = {};
        var legendData = new Array("授权总数", "正常授权", "禁用授权", "卡密总数", "已使用卡密", "未使用卡密");
        var selected = {
            "授权总数": statistics.authCount > 0 ? true : false,
            "正常授权": statistics.normalCount > 0 ? true : false,
            "禁用授权": statistics.disableCount > 0 ? true : false,
            "卡密总数": statistics.kmCount > 0 ? true : false,
            "已使用卡密": statistics.UseCount > 0 ? true : false,
            "未使用卡密": statistics.noUseCount > 0 ? true : false
        }
        var seriesData = new Array(
            {name: "授权总数", value: statistics.authCount},
            {name: "正常授权", value: statistics.normalCount},
            {name: "禁用授权", value: statistics.disableCount},
            {name: "卡密总数", value: statistics.kmCount},
            {name: "已使用卡密", value: statistics.UseCount},
            {name: "未使用卡密", value: statistics.noUseCount})
        data.legendData = legendData;
        data.selected = selected;
        data.seriesData = seriesData;
        
        var echartsRecords = echarts.init(document.getElementById('echarts-records'), 'walden');
        var optionRecords = {
            title: {
                text: '',
                subtext: '',
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b} : {c}({d}%)'
            },
            legend: {
                type: 'scroll',
                orient: 'vertical',
                right: 10,
                top: 5,
                bottom: 20,
                data: data.legendData,
                selected: data.selected
            },
            series: [
                {
                    name: '',
                    type: 'pie',
                    radius: '55%',
                    center: ['40%', '50%'],
                    data: data.seriesData,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        echartsRecords.setOption(optionRecords);


        // echarts 窗口缩放自适应
        window.onresize = function () {
            echartsRecords.resize();
        }
    });

    function chat() {
        var vrl = "http://wpa.qq.com/msgrd?v=3&uin=461313128&site=qq&menu=yes";
        window.location.href = 'mqqapi://forward/url?version=1&src_type=web&url_prefix=' + window.btoa(vrl);
    }

</script>

<script type="text/javascript">
    window.onload=function(){
        setInterval(function(){
            var date=new Date();
            var year=date.getFullYear(); //获取当前年份
            var mon=date.getMonth()+1; //获取当前月份
            var da=date.getDate(); //获取当前日
            var day=date.getDay(); //获取当前星期几
            var h=date.getHours(); //获取小时
            var m=date.getMinutes(); //获取分钟
            var s=date.getSeconds(); //获取秒
            var d=document.getElementById('times');
            d.innerHTML=year+'年'+mon+'月'+da+'日 '+h+':'+m+':'+s; },1000) }
</script>
</body>
</html>
