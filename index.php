<?php
/**
 *
 * @author  Devret
 * @mail    461313128@qq.com
 * @create  2020-03-28 14:47
 * https://sixcloud.co/
 * https://gitee.com/devret/AuthorizationSystem.git
 */

$ROOT_PATH=$_SERVER['DOCUMENT_ROOT'];
if (!file_exists($ROOT_PATH.'/install/six_install.lock')) {
    die(header("Location:/install/"));
}
	include( "public/model/WebConfigInfo.php");
	include( "public/dao/db.class.php");
	session_start();
	$_SESSION['config'] = getConfig();
	$url = "/index/".$_SESSION['config']['template']."/";
switch($_GET['m']){
	case 'sq':
	$url .= "sq.php";
	break;
	case 'sqcx':
	$url .= "sqcx.php";
	break;
	default :
	$url .= "";
	
}
function getConfig(){
    $sql = "SELECT * FROM  `sixcloud_config` where id=?";

    $rows = SQL::Read($sql, array(1));

    return $rows[0];
}
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title><?php echo $_SESSION['config']['headtitle']; ?>授权系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <style>
        html,body,#iframe{
            padding:0;
            margin:0;
            width:100%;
            height:100%
        }
        #iframe{
            border:0;
        }
    </style>
</head>
<body>
<iframe id="iframe"   style="display:block;" width="100%" height="100%" src="<?php echo $url;?>" frameborder="0"></iframe>
</body>
</html>